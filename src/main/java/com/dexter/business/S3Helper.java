package com.dexter.business;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service
public class S3Helper {
	
		private static Logger LOGGER = LoggerFactory.getLogger(S3Helper.class);
		public static String AWSRegion = "ap-south-1";

		private String accessKey = "AKIAUMEKOZLGAHZPTP6X";
		private String secretKey = "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";
		private String bucketName = "hs-pod-images";

		public String copyTheFileToS3(File file, String fileName, String folder) throws Exception {
			synchronized (this) {
				try {
//					bucketName = AppConstant.bucketNameAWS;
					BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey);
					AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
							.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion(AWSRegion)
							.build();

					LOGGER.info("s3Client Connected");
					
					String fileNamewithPath = folder+"/"+fileName;
					PutObjectRequest request = new PutObjectRequest(bucketName, fileNamewithPath, file);
					request.setCannedAcl(CannedAccessControlList.PublicRead);
					s3Client.putObject(request);
					String path = s3Client.getUrl(bucketName, fileNamewithPath).toString();
					LOGGER.info("Uploaded file: {} in buckect:{}, public URL:{}",fileNamewithPath, bucketName, path);
					s3Client.shutdown();
					return path;

				} catch (Exception e) {
					LOGGER.error("Error whileuploading file to s3",e);
					throw new Exception(e.getMessage());
				}
			}
		}

		public void createFolder(String bucketName, String folderName, AmazonS3 client) {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + "/", emptyContent, metadata);
			client.putObject(putObjectRequest);
		}
	}



