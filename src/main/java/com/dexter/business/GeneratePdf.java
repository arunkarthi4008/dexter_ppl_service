package com.dexter.business;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sourceforge.barbecue.*;
import net.sourceforge.barbecue.output.OutputException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat; 

import com.dexter.repository.DexterServiceRepository;


@Service
public class GeneratePdf {
	
	@Autowired
	private DexterServiceRepository hsServiceRepository;
	
	
	
	
	@SuppressWarnings("unchecked")
	public String Waybillprint(String waybillId)throws IOException, BarcodeException, OutputException {
		
		
		String tot_expense="",partCode="",shipmentNo="",SGST="",CGST="",IGST="",SGST_Amt="",CGST_Amt="",IGST_Amt="",
				eWaybillNo="",consignor_GST_No="",consignee_GST_No="",WayBill_ID="",WayBill_Date="",MWayBill_No="",
				fov_charge="",loadUnload_charge="",docket_charge="",otherCharge="",totalCharges="",oda_charges="",
				Customer_Type="",article_type="",WayBill_Time="",Consignor_Address="",Consignor_Phone="",
				Consignee_Address="",Consignee_Phone="",WayBill_NO="",billTypeName="",fromLocation="",
				Branch_TypeId="",toLocation="",toLocationId="",from_city="",Consignor_Name="",Consignee_Name="",
				No_Of_Articles="",InvoiceNo="",InvoiceDate="",DeclaredValue="",VSC="",DHC="",HandlingCharge="",
				OtherCharges="",TaxPaidBy="",SubTotal="",ccCharge="",Bill_Type="",Actual_Weight="",Total_weight="",
				Total_Amount="",Operation_Mode="",Article_Name="",SaidTOContain="",Freight_Chrs="",Loading_Chrs="",
				UnLoading_Chrs="",PickUp_Chrs="",Delivery_Chrs="",Insurance_Amt="",Service_Tax="",TaxAmount="",
				Aoc_Charge="",Miscell_Chrs="",lr_Chrs="",Deliverymode="",Remarks="",discount_percent="",
				incidental_Charge="";
		
	try {	
		
		List<Object> list = new ArrayList<Object>();
		
		list = hsServiceRepository.getWaybillPrint(waybillId);
		
		Iterator<Object> itr = list.iterator();
		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			JSONObject jsonObject = new JSONObject();
			tot_expense = String.valueOf(obj[0]);
			partCode = String.valueOf(obj[1]);
			shipmentNo = String.valueOf(obj[2]);
			SGST = String.valueOf(obj[3]);
			CGST = String.valueOf(obj[4]);
			IGST = String.valueOf(obj[5]);
			SGST_Amt = String.valueOf(obj[6]);
			CGST_Amt = String.valueOf(obj[7]);
			IGST_Amt = String.valueOf(obj[8]);
			eWaybillNo = String.valueOf(obj[9]);
			consignor_GST_No = String.valueOf(obj[10]);
			consignee_GST_No = String.valueOf(obj[11]);
			WayBill_ID = String.valueOf(obj[12]);
			WayBill_Date = String.valueOf(obj[13]);
			MWayBill_No = String.valueOf(obj[14]);
			fov_charge = String.valueOf(obj[15]);
			loadUnload_charge = String.valueOf(obj[16]);
			docket_charge = String.valueOf(obj[17]);
			otherCharge = String.valueOf(obj[18]);
			totalCharges = String.valueOf(obj[19]);
			oda_charges = String.valueOf(obj[20]);
			Customer_Type = String.valueOf(obj[21]);
			article_type = String.valueOf(obj[22]);
			WayBill_Time = String.valueOf(obj[23]);
			Consignor_Address = String.valueOf(obj[24]);
			Consignor_Phone = String.valueOf(obj[25]);
			Consignee_Address = String.valueOf(obj[26]);
			Consignee_Phone = String.valueOf(obj[27]);
			WayBill_ID = String.valueOf(obj[28]);
			WayBill_NO = String.valueOf(obj[29]);
			billTypeName = String.valueOf(obj[30]);
			fromLocation = String.valueOf(obj[31]);
			Branch_TypeId = String.valueOf(obj[32]);
			toLocation = String.valueOf(obj[33]);
			toLocationId = String.valueOf(obj[34]);
			from_city = String.valueOf(obj[35]);
			Consignor_Name = String.valueOf(obj[36]);
			Consignee_Name = String.valueOf(obj[37]);
			No_Of_Articles = String.valueOf(obj[38]);
			InvoiceNo = String.valueOf(obj[39]);
			InvoiceDate = String.valueOf(obj[40]);
			DeclaredValue = String.valueOf(obj[41]);
			VSC = String.valueOf(obj[42]);
			DHC = String.valueOf(obj[43]);
			HandlingCharge = String.valueOf(obj[44]);
			OtherCharges = String.valueOf(obj[45]);
			TaxPaidBy = String.valueOf(obj[46]);
			SubTotal = String.valueOf(obj[47]);
			ccCharge = String.valueOf(obj[48]);
			Bill_Type = String.valueOf(obj[49]);
			Actual_Weight = String.valueOf(obj[50]);
			Total_weight = String.valueOf(obj[51]);
			Total_Amount = String.valueOf(obj[52]);
			Operation_Mode = String.valueOf(obj[53]);
			Article_Name = String.valueOf(obj[54]);
			SaidTOContain = String.valueOf(obj[55]);
			Freight_Chrs = String.valueOf(obj[56]);
			Loading_Chrs = String.valueOf(obj[57]);
			UnLoading_Chrs = String.valueOf(obj[58]);
			PickUp_Chrs = String.valueOf(obj[59]);
			Delivery_Chrs = String.valueOf(obj[60]);
			Insurance_Amt = String.valueOf(obj[61]);
			Service_Tax = String.valueOf(obj[62]);
			TaxAmount = String.valueOf(obj[63]);
			Aoc_Charge = String.valueOf(obj[64]);
			Miscell_Chrs = String.valueOf(obj[65]);
			lr_Chrs = String.valueOf(obj[66]);
			Deliverymode = String.valueOf(obj[67]);
			Remarks = String.valueOf(obj[68]);
			discount_percent = String.valueOf(obj[69]);
			incidental_Charge = String.valueOf(obj[70]);
		}
	}catch (Exception ex) {
		
		ex.printStackTrace();
		
	}
	
	File barcodeOpfile   = genrateBarcode(WayBill_NO);
	
	
		String waybillHtml = "<!DOCTYPE html>\n" + 
				"<html lang=\"en\">\n" + 
				"\n" + 
				"<head>\n" + 
				"    <meta charset=\"UTF-8\">\n" + 
				"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" + 
				"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + 
				"    <title>Document</title>\n" + 
				"</head>\n" + 
				"\n" + 
				"<body>\n" + 
				"    <table width=\"700\" border=\"1\" cellpadding=\"3px\" cellspacing=\"0\" align=\"center\"\n" + 
				"        style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px;border-bottom: 0\">\n" + 
				"        <tr colspan=\"11\" style=\"text-align:center\">\n" + 
				"            <td rowspan=\"2\" colspan=\"2\" style=\"border-right:0;text-align: center\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL8AAABXCAIAAADanwO+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAElzSURBVHhe7X0HYBXF2vamnJYC6r22q9jovZckpyWAXTpSlV6S0JFe0nsldFCw0ARUFBURAekltBCkBggdAuk5/ezu/7wzJ+cicsF7P67wfT+vr5vZ2enzzFtmZw+C/Jj+vyFRliXGCHD+H9Jj9Px/RICLU5QdTkmSZJExkATCPUX9+/QYPf/Hyel0iiJJGUkSZcmBCFlySk4HWBQdHDRIwNP8u/QYPf9fE1dk/zE9Rs+jQJhBSAUwCQD875Bku0RXSArZJstWeuiUZJMkW6Bu8MyKxxAhVqtstsgSkkAlceVTiQYqUyq5br92xnx63429P9zasfb69wvyVyZf/Cz66vLYa59HmL5Otuz51HR8k/PmBdlWwWtHwWXQaax6G9UGlu28yD/QY/Q8CoR5B0xcjLAEpeJ0EiwkCjgddodktcgVdskm2iWTw1kiyaUS4cpuceOlQi69UHbq1yublxxfkbB/9ozt8aN2xo04kDrqcNb47PTwwxmjDqWPyc0YdTx5yJGpnQ6FG379oOGR954737nm6V6t80e/fmFW/6JtH8vmKxVSmWwVUbdVtogSEEvovCs9Rs8jQVj1YGbFQgJZZalCtpfJJptss9llqxl/IAOcdti5ZpaCSSTA5bwjb++Fnxfvmj1+08zBOyNDD2VNPb9xacHhdY6iY7J0RZYLZLlMlK2QXKwekFMWrbK9VHYUSYV5lv3LCxeHHx9U72Lvqjf6+Zzo/ffToQHyrRMOuaQcKGZmUmXD7kKP0fMoEOYGsytisqAnbFBNmGHJYYEEcjhFm0hqjOsVySRf2W3Zt/b8p1E7pvbY9NE7O6O6nVqVUZK7U6645tIwSEc60CqKNoeEcmQzlJEErMmQU9CBYBuUoOiwyc4iWbKgavPp4s1zT0x7/XzfWuXda+TN6CnLFwupDLKzRfrv7rrrMXoeBQI4zJgryJNykYybUgfJFjbZTptcLEsnrb99XbgkMmfUu7/0abp9WPsLC2Ms2Zvl8htAAqBCKgxXUbSL8KbsdtlmFx1gJ0UAHhVgSbQAlHZIMAll2q2yzQr9hGy8GrTCeuP6qilH+794vG/1wkPLHLJDRFYHGueUHYDcXegxeh4FAnow9aSRyHNGQISyumk/v694w8enp/Tc36Pxjs6vnRgXVPJlhnz5JlxuSglrCTLJhKyFsuOGKBbZpApkswAmJFpkswizBRBgezx2yWF3ig6z5DDjKjsJlhLEGvx2h8MkOi/L8mXSihdPzutzts9TedHdZMs1pIDoQuskp1vx/Y4eo+e/TaR02PVfGQ9EAI7roaPccmrHxWUxB8e3P/T+y3s6vXxqzJvla2fJV88gP8SEHcCAJWuHSeR02pxwvCBjoOaAC5tkc0owj0yivVwGO0yywyI7kA7yhjw4UaL/iEhP8voYSRB7EFAUKt38zbn+VU++X6dswzK7bDGhQuD5X+wlPkbPgycaafwvSk4RaxaaBYaHmRwX0UkOMVMzmA6LJJFPzsl6szznp6ufTMoZ3jq7e7Xs9184/ZHx0spMKe+A7KBp5YqMgQwXTD5hEbmJaXaJUSZqIM3kIJw5EEXCySZbgS4AS6xwwhon00qymx1FF0988/H5zxMuLZ1esGmp4+pxufiqdHLLqRnvFIUrC8Oe3zqoqXTzN8IOaiMRdBd6jJ4HSK4Vjf/B8Lwx5uWYLUQTY2bNZrnULpqdEBjcxBULbGfWXVoRfmZUvUO9ns7u8ULuiIDrn011nNokOy/J8i1JLrE7LfCb7bRFDKTcdRbxgCAF8JCZ7IS6gbFrhpsG1KANFcCPU0YJIMgYmDuwr85v+/zQ7FFns8admN715KjAvb3r7O/V7PDb1Yu61pJ6VivvVXf/B0b56lkTGeqAJmnVP9Jj9DwowrwCDzSLgA5C5N2QjrA7rE6HQzY55RJZLiJdArLL13OLlkddnvD66ferne3ytyN9ql9L7G3f8YVcfA5TXCbLN2SxXC4pk81lshMWDBlFVmbC3IXI7oWcgXMNcFig1QiohBSHCMPXbJGLYYsDi+KF3NLNG24sWXAjK7p08fSiLyZcXxNfvnuFfGWnbD0j3TxUcWj1pWWTDo7pfHT6QMvO9ZBzxfD4CbcE9T/SY/Q8KPodeoAbsyTZoENsRSbJXCo7TaR8HHLxLfNPKy5N73Gy6yvXOz99tlu1M1O7lv/4iVx8HUUgIxIx1xr2i4WcI0myOsmcIcFDwuVusgfPyJyh6uHd2+yS6JSssIypQKfddPbanuW5icM3j3zju0lv7onvc3L2iHNzR52aM3xnVJejY/SH+9b65f3nt45ufmlDgixfYPINHn2FQ7YXSc5Suoc6fOxz/dcJs4/ZxehL/K0jJqFUxNrFtBZZLm+5smDc1cHtL3R98UTPJ38b1vTKxxPsJ7bIcokFBrBsgq0CycG2mTFxpbK1hIkvwiT0BhXKir4LMZigPqCLgITCbHD8r9gKDuVkxh6c+NHJJfNNp/fJlguyVCDLwAPkUKUsAdrLKuTjB29+Pe/AzP4bwnuU799BaIHFJlptKIq0IooGqu9Cj9HzwAhwAbNJBtMMkBFbcsO0ZdXpiG77e796sM/LhwfVO5LS/ebBJbJ0wcF2UaBcZKsZi1wSoSIqIGskJ8wVeNhwlUiKwPYWwQw9VP4fCWYz/Q81yafZJJfk/TZv4voJPQr3fic7buEpIFNqhzUEYEnw2CQzKTkLgbLEJpYDTUR2s3T9wrktm+WbVtlmY44bqSwm15jR9Ad6JNHDxgnjhRlwyuixg4SnxAU3LcJKxqiRQGcrDgE+b65ZpBARH/PKu/uSqwyeCzNCs4chZMIc65XmkLQHeyWJZjloQkQ8pnVvhc1K8Zzsl3/LX5F8cljLo91eOt3tpfwRrUvWx9gLNjMzhTrmhC0KWNjp/aeJ29fkJEnQPjC0TWzbEGyVUD7MHjvbcqazOdRz5nqB2SjQTiOekH8kl1zf/sWn0z7M3/Id7STL5RbZAiBQ+3Bh5hCQBHEI90+2W9AC9AoskslU6pDoZSmZWLLNBLubFYkOYXx5p+6gRxQ9aDZGCz23yuVOrBQ7TAhaCFivdhmSnSCFW2ZGYhawNLGEMZK0DMn0YJPPCEEMwd2Xzl2IYYZBgM2OaHOIGEdUbxNFjDyKAkRIPciyWXSaRNppwxq2IgEyEJkLi39ZdjK29+HeL+d2e+rowLrXloyxnoaGKuMzQAjjTSbosJM2BAk7ync4zHa7yWKzWR20iyeJJli9omgVnTZ6YSHaUIsNkoD1n/YAgF6YyDaz1VTuIO9fOjB70i8zusuFx+gGGg+j5eoSX10PmB5F9KCXVswbRhVLzrXymQVANiFkbyVbadFjIWOEaGwYdpj4ZgBwFYYnmFZE/DmqzMwLIyDif3YGAgGIGUBakool6YooX7OKFvKEeSJHhfPkjguLxuYOrX20i8/RXtXy4wZat32LNYzy0PwykaQLGK1FBpTPBSaYkMiMETCzc9iaoN29Qlksgu8FDAEoEEKQdtQmIAjmuFxhl5DgulxhRbFO+fzeqR1Pf55BxUtlJBapeN4fMA8/YHoU0QOtZYOrAVkKe40JDqgNoAUyh17d0Jk4J9Ympo3tQ3BkkZsiiyba42eig+BDjGniyubPER9qNtgomuZSQn2lFokORUCqY9qRAEXiKaaZ7m6evrV+8cHxPY51fvpot+fOfWQs3TBbvnkahbC0rDBrsVycJ13cKl/9wXrqh/Kjv5QxLjq00Xxql3jpiJR3TL58Vi67JjthpsC1R1XosWvOsSJIQNntIvx/WEFsQVXIDjMaKAGW0EnlG2d0v7YlCTiEe2/H0GGoHLBxbgfQnx6EP02PptWMPtugILAELU6pyFZeIZtNsgWKzClXOOUi+DGQQgCXA5afWA5vxS5azaKtnDbwSSPQ4uVT5+ZKIm+IEZkPfyRKTIDDQ+gThwiNQuclLKKdiwcmOfDHJp47Wbxm1pUxwbmdnjjUverZSY0v/pAp3zzKIFfmvHHg1u4Fl7/86HJs9zNjXj/Uu0V2l0a7OtXe3avB3n4tdn3QJies/bHRbx8Z+daREW8dHvH23tFv7Rn39ubwkJ9HBu+a9u6uiI6nFoy49PXsol9X2/P2yWVXCCqkMh2lKFy2VziccKmtELxkp5h+mtqrbM/ngDRwR5ocEtsJVYhFRtdKxqg+YHo07R7qPqbXSnYlRq1Qtp6xHVx367PEvMTwM1H9T0X2vzDro9I1s8sObpKLLpJ/S5YS6QVyH8h5hVog9eNe/n+WKAsTVxJMVbDVKsJ1BqGUErksTzryQ+GqxLPj2x/q/tLO7i8em9zuxsoYKT+HEpTkF2758kzm+JzQdsffb3DmnZfOdn4pZ1ids1Hv3vxsqmnbl/bTR8SrV+Tym5LtRqWAQXs5LNGFYtl8VS445zx91LJvV8mG726sSD+WMeHnST2/Hd/5uylddmcNu7Qu3X5gk3z5lOwoA2qYEezIToi8uHm+RS4uIaGNhpigYUk4w1J0wgp3i5+7rZb/GT2asoe0FXkc5CyUWg79mDO1x299G1zuUa3kw5fL+71Q9sELRf1eu9Snet4HtU8Nanp8/FsXF04q3LFaupxLi/OeRGKFkev+9/T7MUbQJtuLnXn7Cr7LOh3X69CAxvu6vHa4Z8PcKW8VrE8SLx0gSWMvrNi26uj0Xuc+rJvX8+Vj3V84GdokL65X2U8L5UuHZCs5Pphm6BN6i4lCmSCgd2AO5mJjyvEMzjmAgCdw3WQ6eQMnih3pAhwgbs5LefuKN63IzRq/cfQbPwwJ2TTmnTOLxonZS2+tmHpuwQy0lWCIrHYTMqFzzMsnBQ/0MDEKRg1/ilzCGc26Hz2iVrOF+asQ1Gd3/LikV5uDAxqf6fXKpT4vXev9YlHv5y0fvGTpU83a56WyQS8UD3mxYPCLFwf84/QHz5/p/1L+qEYn0gfcWBtbkb3GdvmAbLnKFrerWDC3V2jN8lgiRGOeYDaVyWWn7Zf2mXPW31o/J3/hxAPTum4b0Dy7c7UjPRvmfPTesc9m3Nr3g3z9pms12y/c/DLh7KAWF7pWvdRblTO02cWs4ea9y2VLPpXGiDDBSofNQn8JHphRNIFHVzIMODKjEUkNhF5G6fDGmIl1BxXIFacKd248+MmUY6NbXOj8XH7YG1cXxluObJatJ8kZB+LI9ClyOAolsRwqT4RTCE+VvG/gk9ngUH6oFo1DQwBfEmO3gQuwdchmp91sK5bEYtFZTm2DLgAgOTF7H/SIyh7aCyH1ZbMUXBAvHXRkr7ZsX1TwbULBVzEXF466lD44P77PsY/ePBve/NyQeqc+ePVsn2oX+/wjv/tT+V2qnu3ke6JT1YNdn97V89V94QH7JrxxPHVg3sIxN9bGXF+fWr7rM9P+lRUHV5t2Lyv4Nv3qqujLSyaczhh0YEaH3WODd/Sst7tn3R2dXt7fq9bp8BbXkt4tWTnacmCtfOWkbIFUK7LL1+2EvaJLmz7OHtz8eu8XT3d5JmdS+4Jf5sg3c+lFlrMU1jz8bObYu8ykB0Z2ZsgjgD/yhd9mtDUtCCveOm//zPd/7dly3/vN8iPeL/8pQb65hTDJkAgFCQiYZXhtdtrbEe2iZEVuPK/EpVm8dc52Zk9F7saK05tKT20wH/xOvnIKzznG4evB/IYgIyeYbHYSlzzvo2r30KE3LBQL7cmxrrLFjiVB65INILhMdsIkuimXX5VLLjivn6rIyy7P3S4f/tm6fXXh+gU3v8kqXJt2/YvoM7NGn0gcfCxu0LH4oYdjhhyOHrZvxqA9MYP2Jw/Zmzj4UPrwM4sn3Po64dq6pIpDP1qPbZYLjjOhVeKQy5n1RHqh0CXESiry1+ZO053o/I/zfWsdi3ir7MxaZsFgQmn/hxoHnUEvQyEGyCCjTA+ICJfwLdF1qMM9n27tXt15di8bHqtcet62c+XZhAG7P2y+tctrOZPaFa2Nd+btlG2QlGhMMSlDylghWk6V5f98c+vCgqywvLEhOf0a/db15TPv/f1sB9+z3TSne2qO9X9+54AG+6f1uPXDYrniKhYBeYAOwIf6wrbAaY8S9EhqLvJQ0VBMRAXGCgjCAoKXhcUM+HCbmJQy6fPKycGfymnCTPNNIDDrIx6w8aUIlAnmqWldsUfEIoVptVEiJsptzACBr2M3VxTLN0vJKJdvffrppfdbFHR75nJos7JfPkELUZcJTbaYADRUDXSjFKxwUj1U14MkGybRaaX37Q7TyeiOuV1ftF4/YTHbRIe1XDaBCSU3LlVs/P7q1PZ7eryys2f1o9PeO78synkhWy47e2Hf2hNpYacGanM71tv3XrVjA6tfGtHyVlzXi7P7n/l0RP7KyXnzwi9mhv029Y2jQ+uf7fP88a5VfptolCvOW2wWswO2GQaK0OMad/FRtXswPqyBAD1WMBx1hnfMDGYVTE41iU9MMfxWJ72Hhq6wkISWzA6pwk5Xq02iAy4WiTQ+mRuuDTq2clA2eg+MkgHN3Fny71ANVhYWGQo0iWKFUyyV7Lds4q1iYMt66UZSv/Odq53qXStvwXTZfAW1U3NhENtQMTWOQI2mI5osjQeNHSoV+qfcaTVbCo4d7l3jZs9nbHtXQiJidaE69InJBBt1DlR63PLlxFtDal3v9uKF9xse69k4p9/LJ8Nq3/p0oPXYaqnomNNRQK/a0GA3sdVG5zKKLsgHfrj68eQfx761bd5kWSyywgRnppELPSzlI4oe7hRhKvi5A8y+KNqACbsIptdBZBeSXUkpIDe45wKmDlL36AZ4IXHFBBUCrisLIBnBD1ceg5QMXyS1MPjQ7xK96XZIdqtcYQaCC6+cmNY/r+ezR8JfvZUDkUOvGAsli1k2OenwhUivLlgpwCK9hGDwZqP9IAlrn7a05LJruRvP9GpU1v/ZvPDW5gPb5OICufSGXHxZunXecvagKXvD9RURZ2J7nB8VkNvr5bzw5hcT37/8UfviLq8U9a5xa9KbJaszbWeOyWYrGkhHHpml4IRYc5Y45VtoNrwWLFyGQlP5pVxyPJnJjGlhdg9b2o+q1Yz5xBzCAaXZpLWBXlLT6XUhs0aZ+wkmgWKvZMSTfOHaByFmeaMMdiqUTSZTiVhDJGDghUAv2WU72Cab4WSAzZKlQrKVyvYKtruNMUVRcmn+tdCAm52eyp70tngzDxEOqdzpLIYB4rRLkI0VaJwTA06JYZeYqExMCnsj90AJKwZTbZJL7NcOHnm/9dUPnzP3f+3ae08UDHz1zOA6J8Na5HzY9ET3Rr+9U+tAvzpHJ7x7ZfUc07F9UsUVMuflS+Un155fMDB3UP3D3V/a3+PVc2Na3PrsI8fR72QbEhA0UT4tIZLnGEarzXHd6byJCEIZ1gWJaDq7wQaa8PNoogdE2Gb4dl3vJFcs/tzJ7pA75g4mdcXYfesOOOjMeQWECWHBRCA1O44dnGw4926Dc5F9Zfm0FbBg2GRZ6MIzs+w8ovKBO/gACUKCjDTgusJy9OffZg3JHdX63KiAkyMDjo4NPjaz68VFE0p+nC2f2iRbLzILsDIXSQ3XnVxyvmLbssvpYccHa493fe1Ex2oXB7W6Gtu/fNNS583TGAEu4yDanXDTYMHZbCJWH9agVaIFTe9LSNFjhB5Z9DwEAhJMMmwmMnZhT1mAEcvZE1O75fR+7UT8ELnsot1pNdGLbjNtRT0Mgugxy45yiEaSmsxGtl2QTRdky2XZWgApy8QlyT+HWGq3lbJzF6TBcW8XkdFRIcNdN9G36vJNqTivfNfH5+b03hdaL7vXs0e7/v3M+y9cH9HiyvxR5VsWyvm7aZefqV+UCZeB+SJ0i86TzfBoWs0Pi4Ae2DEkpS2yFWpHvnp+wbCCt6ufn/i2LOdj1YkYeCe0YDH7IPhhEBS00+xwioBJBdvxIyeUrDTJbieNYrPLZosI6CMaupM+xqGNJ9p2ps8AHQ7aBZGlElEskyT2Zp6JR2eh5fTmgjUxedE9jg8NOt+p7qnOdfd1qp3dP/DEtO7X542pWJfm3LNKzPlWyvtFvLbPXnDEdOWQ01H2GD0uIi0lOq2OCphPkC3AR8muJbvffzUv3CAXnTDJ1x30+TetQnJcYW8/FEK9NhuZIcAyrGe0BmJStLBzghaHZJEkSEyT5DTjOfQM6VioLfQNegshWHPom50ZkHYIJhF9IjSRC4riIMwK5eJz1tzvr/00J3/ZjHNp/c9MantqWKND3aof6dJiX9cWO/oGbR7x9qaZ/U+vmy2bbj5Gj4vIoyBnjY6Zwf2WC/bu69XicKjWfn4LBhVmtWwrgmaDewfwPCTFhWbASJfKJXuF02qXbICLQ6qwATJ8NwOKjSkpeAswzFgzoWjgUZGAAUKsEE7wDkXmXTktkmiit62OclGsoGisH3JXUA9QR8uETrxYC2XrLbnislxxVq44J5flydYrsr2I3sU6bY/R4yJyR+FpW2ERwPG4cjyi08GeNYu3wjm3k48MY0eGG0Kn1ZAKc+TK9tcS9JGZzjxDTVmc7Kgh/Dv6HtlKLAFHYCs7+UiQAW7oQC3bmkBe+sEDK1dgwAl7awUrCmxHl2iXAyXRpyCFdPSZsIjRgPgCIvHU/WYQWZ02J53yFKV/Cz0MxDTKgClVxkQiHYNx+cL3Y5oX9IP1BkxODkrkDYRcxWJhT/n//3VivQEzuU4NpEMNZhom887Pczu+lBfbi1CDlOyUVSkzGzFm5NuTIYppYNuv7kLoVSPrGusKpXAN+IMjUprsXCqdm8MMc8WEeYTkIJnDDCFiagQJIK67aNTZ6LN9DdxUDjV1GzHAgh0WFSlCJylB5KHJAX6gq810PpiKZHijF6zQlsAc7Kp/Cz1UrQSwAvhONFd0EFhpg6NyJ4b2BBiYKvmOW/pRB44/GgXRSTtV9LqADn6yXVo0Ea2miv77xIYPzKQO5gSWMjML5JLT4a+f6ttELj6Bmz82hTbXsFpF+mUdOsWHkbBbHVKJZC+X6GwFHb3AjLE9qL+kIw+P/n3Z82AI5dCpSlkq5ZPI5CKwSGjCEvgriFfsQg/JECtt7okF2+efeqtm+fdz6EssG5anK7mbkJydGr2DkLeSgCsqifH/aboPeiqHjh0vYqHfzhcsXLVt6XfZi77NXvxd9sffZn/y7d7F3+3/M7zom70bDpzbcvj84byCC7fs8AvcRDYffXVEW1UQP38JueaW6VDgx+5wWmT7jZyp+vxwo2y5CeEs2WCW8lREJM5xpQzypRtls1dtWfr9oU+/2f/513s/Wbdv/voj89b8+uueo5SUPOkHt9YeVboXetB3tsQwWGzHgEUOi/lKeKGXUHOw0GC0UGeEUDtcqD1cqBt2f64XJtQIE6qPFl4e5lFzuE+D8BeDJut6z41MXX3w5FWOJFGy0m/M/A5X/y1iLizECIXYDjy94rIeWrO1SzXTwaWUwiJXkI7/J0FKOUEOGB1y0hcbhaffEWqN8Kg5wrPGaKHGFKHuZOHpLtMz1yKlw2lib0L+j9O90IMxYojB6MKsEWEr4qZl3yyhRaSm7SxvfabCOFtlAGep/xTPUuvnaAzzFUFZKm2mjz7dq2Wssk28ov5Q79q9a7Yfm7lyB/cVyHP+7xOkHfkZLIQayX2VK/KSu+aM7EynwICQCqguaLR/NoZEFIihp8fkRd7NZ6r0c331Gf5BWZqgpSr9Z0LdMVtzr9GqozNij9FDf2HhAj000IU2yaflYIU+xssYr3w906ttpiIkUx2crtan+BhSwRp9qoZdfXiAMX/kY0hTG5O8tFGK4BgvfZQyJM4rONpDH+Xdfo6HIcszIAXr+LWQMbtO0Bs7miQsdOiIO4kQzK48cFfiT90Jbg/wMF2Zp0GqCK4FUAGlKVryf+3/TNm6deg5dBj7HAHu0z8xQP4Je8cKy6zxexNVLROweHxDYvyCZ/oak5Xa9KotwqHwSP9aC6GByYirrN31x0VUbWXc75/8M57//ePTe9BteStv3Pf/fPDg6D6ai8l2mLeSaK+A/5Z9+pZQP8ynXbJXQKyvMV2jS/XRJav1Saq2WULTmYqGUxWNI4Qm04Vm4z2ajfRqMt6ryTiwR+OxjMeomk7208UKgZFKwEs3yyc4ybvtdAXG3ZChMGT56tL9Wk3zqPHhN/svU+2kwsqhJ9gpXXLY4LY5aYvMyn5EmI7+ICDyvY7bmLmzYEwx7biyI4Fk/bKdD/hUtLXGXGzEwSVkXiChp+zG0Q2bBreWC6/SDzYhAeaenpE5jxKoEPrKgxTc2Qq7f/NB/kGZSn2WV9soRUiUApI4KKNp52ls5Cro9QGpRljXVmAURaAwNp648AJxRSNt6AjbDUAj0VTUgkcsLdpLzjUgjkox9mgkXFO6pRjW08oOoknuvGSu80ajFM5UEwUQRDfv4P8RqO5jNbMemllFtASzVuwT6oxSGpM1umQfbZqvNtVPm+SrjfcN+Khf1Nfh0SuHzlw+MHrVoOjPhkR/MjT6s6HRn+I6OHLpkKilCPccu6hRh2lVg4YLDYZpmkz3D0zRBKepDQkafaLKkKoKSla1iXpSF6luMOjMLfpu10mvukUMDExQ2t7CHzq25+44m4u7Mh8Wnux2dj8lRlkmUbSzDTXcF+9cPDlvdYoslwKYgJhMX28Ci2iCKy85ZpgCWf7pcL5Q4wP/gEy1YbZ3cIwiOFqlnyM0iA1P+ApPYX/zmcIsIjM1kzErhO398lnls4l4Soq07Jaz6ylAbwZWWC4URAMAaLiK4kxo+T2zYmlFkKPDmXZ1WAW83NsZkf853Rc9UFhYhxhqQk+3kQs8m05RGxI1hhS1LoUwpE/QBEbXej2cp74voePXZHnNzuNt+yYoao/2DZqj0cer9fFKQ7JSn+IXnOaji/FqPlo/KAYLiiQDnROAF0bfiUr0859WeruMaGKaYbbgfs/snTJj94giMU/JFytfimWys8QhWgmhaJn9+uoJnWXrZbtcYoJHLpXLjhK7ZLFQk5GemLai2SqKWfiTov6oJ2HABaarjdFqQ5SPLk1oMGThd7tYJ2EvsWbQqwXWZ2oCph1NQgwKASAAB94F3kIUC3a3HPOK7OVsT5D2jugEpdPi2n5ELs4cSq7RYBmpkMr9M3qDBanGtjmREkW64OJm3KIB/zndBz1QAew39yT6NkiWa70xXd0qVmNMUkJbAUD6JD9DvHeLKV3Ck5BYchbL0nX6NQiMMu1yg9EBEvUupgNTMAbYEMjygCmfw8z0Mcb5BScoSfykQIt56xN92iYK9XoculBIZdpLRScUQYUslbBjApj7B0UW0V7iFO0mKDSYKldPHPlkJoutMNsLJPohUrT57tR51AJNsxlPBGb4BCVp9NG+xlhfbYLQqONJ07/MItKHCXjKIe4agT9LaCC9+v/PCHU5JaiQh4Ie0uJsr+dCuVXZaKSfLk2jS4So8DYmaYKTffUxQr0RyV9sQQIJbXSamJzkDatcIm7GCsBaom/RSxzOUqj6Gm9O9AqY4ROS7BucoiHxk6QwJKtCkoV6A1OW/Ewt4L/UxnT78RvWdTvOJn3y89TM9V1C53UYOrdT6OJOwyp5OGMKL+o0fBGuXcMWdQ9b1HnI/K7DF3YaNq/TsIWdhlJ8x6ELe4/9vIwtUqvNxk3z0otnTGezEbTT7y+UsrGWYzN/eOvDlA7DPukw7OMOQz/uEvZp1/DPUMVTgTN9dcl+2kQfXbyPIV6tjfXVx2uCprwVvrxL6OIOw7LeDZ/bMXRB5+Hzu4anvN13/I+7DqM0YOeq1bn56JXZq3dFL/jhg48+6TA0q8PwOR1CZ3cMXdhx+OKOwxdRIHRBx+ELOoTN6Tgu5d2wWW/3nvHtBhJpH45N6hQ6q1vYvM7D53Sm8okpGQZh+JKOoR93ClvcZQR4YdcRCzsOyRgZvyJj5aZ9Z24gr9VKr0Z/Dx3wX4ienw6dFGqP9NHO8tElkpAISVYY4/yDo4WaQ7advoZ0DhumgzKAKSsy/Z7xkCxfWn42yVqItg9LWCU0n6Y2pmowE0EJProkyDOoMI+WMzsMz6RiZHnrySv9oj5/rf10dYOxQs0wj7rjPetPVTaNVTSL92occwd7NokRmkQLTSKFxjO9Gk/RNJ0mvBaubjLFq2mkd5No7yZxiqbxiqaxQo3xA2LXUqMI05D5EJyiZLNY7LKFfvWCjK13hi32qh2majrJq3mSV/NEsHeLJKFRjHfzRLUuQ2NMUeojNcYYNYQl6XHIznTPxvHKBrFeLWOFljOFFjFezWIVTUZ71Oqz7sit+C92NO063b/lYKHmII8GHwl1J3o1jPBsHO3ZJMqz6UwPNLspOFpoGiU0jRSazRSaTRdaTlG2mCq83GXDARpfVb2e3o2nKBtHqZpGeTeJ8moW5QFujvQRjJEr2rNFjGdzYlWbqUKj8ULDsV61P+wSmoihppdwBJc7GBj6z+me6MHAkuaiD05wFzFnmdBwoo92jq8OnmqSlzFR1TbZ3xDt0WAYNAo5RnbgjKx9B+GHjo0wkcO1MoUddMLcDkOUzjXZ6AOXBeuPCY2mKrVwwWB9J/prk3z0iUCPQptmGDB34+Erjd8bI7zUWWj1kUof46uL0uhjlfp4hTHFy5ghGDO9jGhGAlrCOMHbmOBpTBTaEXu1jRUCxr0+fc3opb8KjQd5h8R5hyR5BSd7GGMV7eO9Q+KF2r3mf7ubemkzOx0OtIqOXNEnlvKxa0XPakM9WkQpdGnqkBSvdqmebVM82yZ5tUvwDI73bpvsGZymDElR6CM0wdFKQ6JCn4o2ewfH+wSn+YXMEozxwuuxQtsEwZih1M/10yV5Nxnv0WCEb+sZGm2y2pCm1EJsp3trk1XBGcrgTO/gNMGY5BGcwDjeIyTGIyTaIwTZP/EKmlOl9eirsrzht0tCreFK3SwvfbKXPsnDkCgYkwVjiqcxFT31ahvt2S7Ws32iEJLkEZLiGZIm6DM9gxd46xf66dKFV7olfUp7mFgiZEkSYtxMM/sf0z3RQ4WTl8g/Pek4OENoHavUZVXRxvsZImCpeOuzNAERDTtG4KmTjFkSL0zK/AsiDBHgydOWihHxyXdHhAYfqXSJan2CSpegMSSpwbqEKtrYJwMmezUKF1pNwVh4aBNV2hSVLsNHl6oJivduE+kRECW0jBCaszXHuVkEMdZx/UkeDUgOCS9/GPHxL6ilfd9oodE0tS7L05Dm2S5SGRKl1mZV0ScINTrkXKdmOCx2pxmmJXVz3Y5cdZPu3q1nKHSzfQGRoBkaQ7q/IVloEibU7iHUG+DTKkYVHK80xlUJSPXTATTRqraQmmlC9clCzdFC9UFC7RFCvQkqbaJGF+8VnKTAMoNeMyT6BaeqdGl++tl+2nRNmyhlq0lC0/FC08ms5bwXLODimZBDHnVmNHpnHFoVv2S7UGe8Ro8RSPMxxCqC4wUIbH2G8MoYRZ1RQo2hQq0hQqNRQosZqqBUjW6WRp/kqydke+mzvFpGvDeSftfHAdHPBMGDovuhh7ZN6OMC2CjVdSO8ApMUullVtHF+hkhvTHPbOZ5Nxg2JXYO0dtu/NBj/SeTAEnqgiJ0irGB5UsY6odkU76B4pRbjS9CB966mrUWqxScwAkpN2Xa2xpiuhFkKxNQa9rfA0c27zng3LHVQ5NL+ccs/jF3h4jji/rHLh01fOmj66oHTv+o7fsGBs6RSoSqfbTNc03qGly7Do12alz4es15Fm+LbMuK1wNGFdP4AwEEqeUbmOs+XBlVpnakyZHiHxCiDE32M6b6BMzzr9Os/bcmnG3Pnbzj1YrtkhS5GbYz3D0qB9aNsCzM/zrPJ5Kyvjn3+bfbn63Yt/D5n7tYrTxqn+Gknwy6EJecRnOERMttDn6rQJgl1B2laDnjtvXEBAyK6Ryzqk7iiR9zKD+JW9LutF7wjg6KW9Ru3fMEKWgBvDprlGxDtrYvzMcT4GCIBR5gNyuajV22/uG7nmXU7Ti7fcHDht3tDBqf7tBqn0SZAhPvrojUGaIkMRZOJPUYmoxCI2L8QPUTwHcnNyb1Q6l2rr8qYpjRk+uriNIYopTFF0zbLo/7Qz346xJMC2jzwL4npL9Z8hyhVINBr4qderSNov1GXAAMCAMIaVUDU6+eotEm+hiTUqDCm++njfJqFBfSO2ZBztYKVdE/6p18myaV2B8xGc15BRZWmA1XaZMEwG/rCXxvjb0zw1871qRvVtm8ylkmBLL/eb6ZQo9+TgTDsPvfUZ6leTyCt1CbSu0GvL344wguEun2y5XjvwGiNMdFPm6DRwfhL9g6Y8tLrUTwBp56TF3s0HVbFONNfG0dKNniehzFL0WbK04EjPlm7q7CEkPonyDXTWJfPBoxWaeO9jdFqY6Q/xl+XogqMeKXtSJ6AEa1KE5oXMMYrMEGlTyfo6JNhA8B0Gx37KaX4K9FD+xGQFeRyy0t+PCDUGabGXNKLiFiVMUoRnI6VpG7Q//C5Wzz9fVsm0a/zkfMokY4gh7hxxxjvNlGkrSByKtGjgl4PTlEZYlSGaGVIosIQKTQZ2nfMfCoCQ4SpJtuK+XZ0UA4uks11hUFOmwUmuHX0Q4NAM9giOUpLkfXH/WeFmgMUQRBsQE+k0jhZo09/InCBol7YkNSVtTp85Nt4sloX4dM+2gOL25juHZDq2WT833Thu/OLqGoRuDXvO35dUXeYxpCi0cf76mMVMHcgUZqP6zz6Y0pihycnx8z5WqjZVxEUpw7OUOlhmsz2NM5RtJr+auCQS5fJ2kPznbDUueeOCaWdGvbjzK5eUEckZ7koXqGDWZLzYqkVLVfD2DLCxUv20WbA+tS0iukYNotKQwkojn7qxXnDLCoajfLQz/YyZBGsjWkICHVHzVm7Dynp/S4m4MEB6J7oQctoPwqAlgdErBaaTPIMjFe3TVXC8AyOheXoFRD/imGEy6X+EySJdtfHZOw9aH6J6N8sFMYBKSw3005SIsrXGKC2oj0Do1SGmc+GTGBiDfoTjPZAuoiwsyjuD0TWmiuIRCbRZJNssOhp2hZ9u1+o3t8vMMbPEK9qP12hj1YHpVU1JHo2H6sOjPLVzfXWJXoaxqmDR1fRz/SsMzK4fyJkEqomvWwnCyn+4++FBmM12kxffZzaEK0wpnnrZgm1Bs1auZmqk+VzRaLwjy4qY6rCON9Lm+YZnOJtSPfRJavqDV+//Qy1yUVoI5JzZsr8D0S9sNJl5bYDQt2RSn0mxgeWH9Cj0c8S6o5JXU6evJMOFcrshzHkXw7mCzUHKmAd6mJ89dN99BEqmHd1hu06Sb8mDvTQRxZ8i+JB0L3Qgz7ZoWtEGvcGHaMUgfF+7bO8dLAGYrxCYj2MGQptfLv+iTzxnyFJtNL5QnTVRmBYv/uUR50BUE+3o4eZPklQjmSbw1LWZ3m3jnp39BebT15bdyB//cH8H/bn/ZR97scD578/kP9T9vmfs89vZIwAD6/PPr1h/+kfd+ZcLixy0pfCZofJ7rBY2fkhOTxqhVAzXB2SqgqBFoa6jIUf5KMndeYVEq8IyvTRzfUNSBCq9x6buoL2FjC9ol2yW+h3SGT5vTFzPFoiy3yoLbUhwjs4w1s7W1l32LajVzgI+kyZL7SY4NV+vndgYhVdpCI4WqOP8Q+KfE4f8V1O4bfZeT8eOLcx++ym/ed+2Y8G5284cOGHAxdY+xF/zn3dkH3uu90nj2TTK7+RWUthHaoN8/z0cCnI9FEa4duHbjnNN1Rhtkk2Gy3yiNkrhOeDPRoM8a7dW1m7q2fd3kKtAV41uhXT141mBp6/Cj2QOzTVYgnWu6reMLV+jhJ2vjEeI46B9jamezadMjmD3uzcttrdBNzRbjnbIqTHYEgLO+SmCOFDJvbQyGUeDUe6EHM7epjPBfT4wr/VpcO2UDefQmeJ6o4UaoXScaJaw4Sa8DKGC7XhB1VyLXCoUGuEUGeSUCNceNK470g+anHYzQ7ZSXuC0GL0z3jK7w5N8Ww4Ta2dpTak+BgSNNo0tW6OtzET3jK8aM/mKer6Y5dtzuGdEJEZrSaZb8b8VH8jRh2U4WNI89HHqPVxCkOGUpfyZKuh1+k0sO2qTfZvOcTLEKkwxPkasACifPSxPrpEf21i1cAIoc5Qof4Qod4Qof4wod5woW6oUCeUHX4KF2qPFmpxHiXUHsMCY4VneoVNnItWNO02g71azvTTxRJk4UkYYv1bDWKnouiNhyQ6rLYKs81WZLIW2uUCuwzLClxkl2/a5AIyFZ2io4Krrb9Kc2GWaYPSfOTsNeHFgarAed4hiRg12D1ewXGqkFShxvB1JI25+L2DOHqc0HwcOiA7nfFHByDMaB37Nu6n0XK1xS2eSvToE33hveuTlEaySZWGZHorYkiExoSfrDRUMsLGOJWBGLcK+LGGGG99gqdhsTIo+fmAUSX0XRbgb64QLfS9CRYB/fQN2VsvhSSqglAg1eKrT9bok71RXXCqd5sZ9brF77lEUspiK3NanRhr2kyk/VJTfpHDr/5E/6BZGiMEQDTBGgI4YEbT7lN4B7/bc16oO1zTLkGpm+4XHK8G+uEcGTJVuoyq+oQqegxasldwMm852kyNRxcoDOcoEUwBI3wFBNKEuqN/PPAbEPJsywleemjJVHhwKmOEh2GBZ6v4Vl3pvQpX4hAodrvDbsfyJKOSXB3XggXT7JDMcT5Ii4fTvdADYkdUpPSl673qj1PrFnjoYDHEavQJCnozFedZd8hlkpfUAZ7+d/TPxoP5H3SPvofGw2UbDsPBURuz4Bfcjh6E4YLBVvAyZnoaZ3nAc9YnqrQRauBJl0U7GXdjtTZTFZQBVmozPPXJHq2nBvVLgTQQ6adZyGeht5YW+ouqk1ftUbUc7WWMUQRj2mBgxYBVwTHewbFVW8To+tCsWO129i8vihbZjFHnlskPe0951BzjF5RJ+zd6crh89RlezSaExXxGj2U5at56j0bjvbUJfiGxKggeYzKEtAAD0QC3P65KQKwyKE2l/WMvMjWwVPScU+AYIqAKSvKs2yffZNl/slBVJ8zbEK80ZKix2Iwx3oY5QpOpw6OWU5X0cTmUER1b41qJE2sOkeu+8qkr9gHRvdADHQOVj0DvcbOFJtNUIQtov0EPRZ6kgJDQzqwWMoYZs1AHfwA1IjDilWuAiUuR/Ah7KeJgj1TThfsEwXGbB7kCYeNGDxdFcLuUhnQFbcvGqVqO8W8xuEqzIX5NwvyahIJ9K5nfMg7zaTzct3GoL66thgivvDttDlOp9MMtkDwYYUDHCenTfeQnwqvD1JBSIfCGkrHKNbB/4UVieozJPkGz1fWG9A2NoR7YnXb6KLMCo87RMylrtWeDSZrANBIPhiSYZb6BKUKtIV/84Nqz6D9+qaLpDKV+FgqEdQLEw3z2MqT66qKfaDriqUb9qzYd6tcsjNr5u/azfjV1sS+uzcI0DQbWMXyAMud9tdujzhhNcKpCnw5hhnYqYAs2Hrfk2714yr5mIQHJ6mfjzIjfglz3leSKfUB0T80FF4k+6Jartx2nCIIzMltlTICbqtZhjlO8W4x5M4xerdvJnflDsxDBxQ1QQz/dxE4P0PsAon5Tl3s0CNUYUzwh1QGXO9CjS/DXRfkbEny0cYpmY8Zm/nTVIZ4vMeeXWDhfKDVzdsdURuJquVRmPVtYUUKCBmKnXLJZLTZ6AXHZZK7/Vqii3pgnDQu9A2cpIbEMGT66JH9dko8+WaVLUYfM9jDOVwcs8K0+IXXO99RQOhACi4ed15Fl48CZmtZxcNOgJaFJ/bTJYKHmoJxLcNRpHXUcmO7dZIZCC+coAfLMz5CqaZ3hE5hU/c3oCxXS5SLTxRLeyLu0P7/YxRdLLeeLLEh5vYSGq/fUTz0bT1MZkrGW1Pp00uNBaV61Bx3IYyYzq/dh0b1ljwWNO1to0TQK9TBkCsEYbiypaDUdKUwTGgxMWPETocZ5N9mDGGhfhnYHzB3mvIBgUPSc+LlQJ8w/JEOJOQt2bfMANG6GUoCCJ+tHnyjUHZq5hvYqGGEw/ww7GGhl0Ub/PCdv2YYjZ/0aDPZtFVW1baaC3qbN8QtMfkIbo2452TdgWlVjnMYAU3SWEJKsbPuJos1c4ZV+y3/OpZzUC7J80P4n2/T2CUpWBqQoDFGa4BgffbyPLgEm8JliPCR7fFzCCq8mU5X6LLUuGbLHVw/vOlPRYmqLnnFUVKUW/0ODwTyeWv3PsINc07qdpqsCUphhlKTQQeyl+unTXwgaRyYDfHCJnS95SHQf9GAm1u045VEvTDBiZGdpCD0QyOk+WqBnwP6rN11J/wQVmKxZK36s1S5M0TgUVq3CkKWgt9Mz1HCbDfCtEoVWUbB4gB4Mk5KJJaUxUxMwNaR/zO/WF0bYPcg8/DvGoJc5nDYzzEinlXY7ZTnzy01CtV5PtPpEGfiFALXYbqpKG/20PlJ4tU9o5qaX357u1Xw0DGFVcKoyZJpXyDSPtvGq4CyhxtDsy0C76DTRjyBfMVuFWm/76Knv6uCZKsNM7nYJtQf+cvwab9kXPx2Fu6fSw7IBQCGkE+FeqNtOVzR5/8R1ejPjojvbjCgACBCEdYaFRlsaxE7TdavVu/EA2EbM0I5R07vVVE1AkqF3KvLAKiOxzop8KHRPq5k+UpEnz14vNP5IqYfYhGaJoTPIunkK7Rxlm8QRcw5MX7AjYu5PUxZ9P3nx97i6ePH3kxetn7Twxw+jP3995KzWg1KffXOi0KS/R6NwvzYzaaNdH6/SxvnpoaES/EPSVS2mKeuGDo3dqmg4EoaOV3AqvcrRxvobU6sExvs0GvP6wI9/3HPuWnEpt8/dq42HOfNZYDay3SHabRb6mW1Mxbuj5wg1BvgbI9TBsZ4hiZ7BST7GzCqB8zxe7v/ZRnrHvu/EBf/qw/0CUmFHw3AmhwBaSZ/k13rqc4Hj8+gnEYjO3bIIdQZ56TKfCop/Qpuk1mX4GrI0ASl/08c93XrMkk1nCpzyLVl+se1oP10EzF61drZGNxuKxk+f/ESraS8FjJ67cte5m7YiMzX/doHDmRPWCQYd7CRDzfHT8atCjTCFfo6Stvjj1MZYeKOKBuPHpX6HxPRvraNxKOgh0T3Rw3Rq234JijbT4RT46ZL86DAUPJRoqBs4ukKDCcp6I9R1hwgNZwoNo4SGkUIjxhSeoW48ybvBJN/m0b6tE1StkvwCybPwNiR6GZO9DclKbaJfUJpvm1SPekNrvzVqzzkSYyEDYrwajlMFZPnC79Cm+epnwa9+IjhO0XK00GCIUKeff5sxTwdPecYw+Vk98dOGCW5+xjjxmeBJzxk+qtagy6LlP6G0izdt9YzjPeqNh/2hDI5XhCR66GN9Q+I9G499us3UfedhQ8s2tpO+/PvjkDQqYyTbAkiE3kFnn4TwaDym0XszMI2iwwZDyqNRb0VImlKXAd3trUvzRsAIdy++qmGmULd/1UZhr72R/FQwVEy8tyFWqZ8B21mpW6wKWOQXsKBqqzRFnbEer/SFdf+PdjOfDZ4KftqALoCnPK2f8Zxu4j904/9mmPRkyPSngkYaek5AwyIWbxQaj1OQX5bqo49ThiSqgxOFV/tuOHAOjRKlEranhoQPh+6JHnaU8nltuDooRoWlRsfg0/30kX4GgAkwStXo0qsY0jXaBDobdTtrE6GM/NpiuST4BydrguKrBqf4BsFZSPEMyfAOSfXCMgqKFGqG+jQcn7x0I02PXGGVi29Jsr5ngvDSSN9WCf76DJUh09OQrmyXomgf421MUujnegZmKLRZ3kGzwApctSne2mTOnoGJHoGY+BSPVwfuulSx6bcLf2880LdxJIwPQRvjFTILRrEyMN6rwfAmHSYUOGjM6ecL6RcNaPGOTl8n1OznA3NYjy6k+xhmqYOSnwhOUjcd+/4I1zm1DmOShMYjFAZIlEyFPtU7eBbtKRjTlCFJKmOsok2cV0Cyl36WpyFLZZyl0cb7Q920jfOCNxcMQztZYYxXAKAhaULrdK/ATDDvCNhTl6oKTFMHzFIEzFcb5gn1xvaPWIUa3wlN9Ww9RaFLV+vToNA9MBpt0z1r9rpWhoVtop9xIlX90Oiedo8sH7xUKtTvD7dWRburcK0hgTL8tbP9dHM1OojTLA9jhmdIShVdclVd0u3sp0/1MM7z1MNNy/LFAg1I8glM1rSK92g4RagT7tN0UOseMz5en81fmDvtNslRLjnpJwgwkylLNz6vHeFRv793y7FKQ6wnlnK7dFhC/obUKrAZdSk+9CEHXDO6+miTOaNGsCYo5ek3M6Z8tVtoGOKnmwQxpn4zRfFGJHwWVWC0Z93wITOXoUb2bwQXQsXR9pPN4WD/9FqPsYuFhuG+sFRosjMIbYZkjR4++ZgZWeSCIU+zDpM8aoWqW0T4QqVCPRkzvYEkTKoh1actTJwkRTAM20xF6/ia7yz0qDVCaDJSGTBdGZTghZUQPEdoO99bn1bVmOpvSPEnmzoJTp+PNlFhiIZXXzUwuUrQ7CeNszzrDVm25RhqfEEfptDOUAYnq/QJCmOid3CWOiixTtsxNGp2+u0RqPKHp7juo7nk2V/+IvzdKNQbLNQdLNT5UKgzgB2AmiBUHy/UGCnUDhXqDxQa9qe9dnqTcDuHC7Wha4YKNfp71B8i1PrgiZbhzd+ODZ/+9ec/5Jy+Rf9IOfDpdJbSKED4WGgTEbqe/eqifMMpf7xhR5dxGc/qhwv1B9HbidrDhDpoA1rCd/qHCfWHCvVQ0UgX81cZTdC2UcKLnYQa3YTXQoVaM4QaiB8g1O4rvNx5zpqDKJx2/8RiOpSB+si6cIhSEX3+J8vV2o8VXukk1EY3Bwq1Bwu1+wkNRgg1xwpYLkvWIS9c/3nLdtQPmeyHlrzaR3itPzWM3pwMp9cjNcPZ+5MPhedDjhVJ207eCI1Z2+CtSf6NBgo1PqQy6wwX6oUK9dByzugICzQaTCNZB0WF0yfbf2+XW2w9fqFIeC5YqPeBUA8j/CH1ArU83/29fnQcj1ruJLPn4Smu+6En+/CJT3/cuXJn7qrtB9duPwD+clvO6m1Hv9qa883WI19vO7hm+8Evtx9Yvuvg8p2MEWDhVbsOfPnrvjW/7v1xz8Hc/EvXy0iuuAkdpn9Q0W6WwOybS5G90qAf44SrRP+MmcvNQsor5dKhvGvfbs9dvf3omu25X+7IXbk9Z8WOnOU7clZuP7qK8Zc7wLmrdxyLXfLz/LX7v9ycu3bbia+3nVzzy9Gvtxz/ZutvK3/adfQCnSShDVebw2miLxzo6BvbyYSJaqN9B/lqhXnt1n2rfz28dvvR1b+ig4dXbTm0YvuxVZuz16zfLNpEK/2mGxGMqs3Zp9ZuPfDV9iNfIeXWnG83H/9qy/HVv+au3X541c+/UtGVM4uij1+8suVI7pptu1duy161I+d3vD3n853oRc6XOw6hX8vQtY0HsboKrpeu+mkP4tdsP7Tu1wPfbT301ZajX23O/i3/Iv1SBFI4UQest9+5pH8l3RM9tNFMk377xP/nRNIG6wU6ooIOQbOdT9pMIbMPf00i/aMQzB5BtXQICIltlVtF/1YTbl+NqPWfeWkHi76logGnd8BUDX2dT7YD7axD7LnAcQfxIgAztMdKX7X+rtg7yJWYXhCzmUXa21t0L7otnc1CFvHdiL6XpQ9qqY7K34l/OHQf2XM3co/abcOHIOfb6Y7bB0W/q+s/quN3mf6jEu5O/2ZRSM5zPMAm/LV0H/SQgGDkur8fuVIzolsWwR/9Z0TyycWP0Bjzxjgf3G990DAxct3/L6F7oQed4S9m//wwIQvSc/p3896VkJ0TinJFPQKE9uBqt9NpJR7zPyQHO7j1fwo9IHd/0D1OfOA4MZAQcdDw0UQaBHCLBFar1fZnvrX4A7FSqVgUhRpRpuvBAyWUj8Jv75E7BoQGoF60n9/ikSsRax5vGAi3PBd/dAe5c7kDrGeuEUMuPlwIuON5sv8VdB/ZA7JYLGVldNgbhE7e3j2egBN6jhiMNR8C96wjwBPfQTzX7eR6wMgVxYiX/MAJJfMrD3DiHQSh5e56EUbM7c24PcHFixdx5b1mD3/XeB5zByExJ1THCSl5Lf8qi5t4GpDr/qHSfWTPm2+++fobr0dERLz77ruZma4tV07oALqNIUC3ef8RWbNmzVOnTvEEjRo1+vrrrxFAGn7lhJSuECM8uj2Gl8NLxhXhwMDAOXPm8BLwlNPtBXLikfwpj8EtJx6DK8L8EcJotnvCQDyep0c8Dzds2PDTT9m3LJXyj6fkGUtLS+vUqTNx4sS5c+n8KCvgzu7weJ6XF8s7BUKAh0NCQhYuXIgATwBC+PZCQDyGkzvmodN90NOsSdP58+bxMNAQExWNADrAhwOEEcSVdxuBZ555Ji+P/g0iUFFREUYHsgeP+DAhjLwIsHEjQriiwvWFFhdXPMyJy63y8nKeEU9p8FgJ7vkA8WQgpOQBVrYL0Li6xxqaFFekh4zkTyFZ+ZUXeOXKlZSUFARwixLQNgSQkj9FRl4UL2fHjh3NmjVDAIRHPI1bUyM7Ynh6hPlAISNKu3nzpnspIg0eIRdK4FkQaTLRlikI6Xm8uwucEOMKPVS6D3qaN22WnprGwxfOXxAEwWZ1jbvZbMaiwcoLDQ3FLe/PP/7xj7Nnz1JqWX7vvfdWraKXNaAGDRocOuQ6gNe2bVu3fIqPjx8+fHjHjh1PnjzJY1avXv3GG2907ty5e/fuPAbply9npzBleevWrbidOnUqYIq8iOH1Xr16NSAgIDIy8vXXX+ftcQ83b21OTk67du0GDRr0zTff4BawwAyh6j59+iASMUhfXFzcuHHj2rVrI55yynKnTp0++8x16nTlypWQxP369Tt4kDasCwsLg4ODn3vuOTT+8GH6iQzQ4sWL+/bti5acO3cOtxwKn3zyydtvvz19+vQVK1bgFmZAq1atXnzxxaFDh/LGDx48OCsrCwF+m5GRMWbMGHRk48aNuOWFBAUFJSUltWzZsl69egUFBYh5FOj+6MlIS0eA/2Sql4fnoYMuEGAIvvjiCwR8fX0vX6YPR0CYVDd60FX3rANkGHcEbty48dprr/FIrF2gDYGlS5e+8847PPKrr76qUqUKphyzy0ezadOmSMCf7tmzR6VScZEAKLuFDcrhY52QkAAliwBK4LjhV6g/vtxv3brFY5CXB1555RUAgqMtOjp65Ej6QJPfGo3Gzz//HAEQkgG7CKB2yAMEdu/e3aRJEwTQVBSFSOAPtwBcz549EQBlZ2f7+PjwMBLwGoGD0aNHI8BvO3TowNEDgq5HUxFA39HBa9f4ySEZaONCsUePHgAij3zodB/0tG7ZKi2FHUQCeiRZpVDu30cn/SBRPTw8MMTLli3DKkEMn2kgyY2eNm3acNmDZBA2Go0GYQiM1FQqEIRFP2MG/Uv0NWrUwCjzSGRp3bo1D/NlBxS6BQAAh8Q8jAK5hIcYwEBz1TB79mwIAwR4XhCfoe3btyPNpEmTEHbrqTNnzsycOfOJJ56AZKKksjx+/HiIBAR4Logrd9WQHCiBW2A8++bNmyEJEEDfuQYEzZ8/H7InPNz1Y2pdu3YNCwtDAFlQJh+lqKgot8DDFQ3mxYJatGjhFthYNkuWLOHhatWq/frrrwgAdtOm0a8jojTeyIdI90FPi2bN05IJ8oDOzRsFGD4bfZlACxdAGTt27KxZ9DEsPWc9UavV+fn0FRUIgwhpjwAfo6effvr06dOQWHyBgj744AMgD2qCrzCeDGMHA4vf8hiAiZuuIIAA48jDqIvLHhgraBifPyxQjh43oWFcVEDgYwXzaQMtWrQIKhKBunXrHjni+kwdMnLIkCEIcHwAPVz28BKgXv39/SHeEAYBPfXr10cATwELOF9o+fnz5+FkcAiC9Ho9n2wueDh6kKB///4I8A5CP7rNIKwNd2eBJGCRhyH5uHCFaOSyB0Xx7A+R7oueZnNnu5ZF+3btoiIiEeCNRn/i4vhxXdfCQsDb2xvDxyPRea7p+SOIGainbt26sYdEa9asgRriYaThE/bll1/C00EAtfCxvkP2oF4EkF6pVLo1F5C0axd9lgsVAJuJR3JyzxkoNze3Vq1aPIzsPABIuWUP0DNw4EAEeB+huXjV7nmCEcMxB9qyZQuMJAR4+ZAKU6ZMQQBmGVfToAULFqB8HgbxlNCPHD38FgaiGz2AGsQVAmg2loR7Kb7wwgubNm1CAFJt8uTJCCAvH9iHSPdBT6sWLXv16Ll29dpuXbtGzKAPnaDC+FBeunQJNiOEMEw8dyc9PT2hDngYNgEXvNzBgaOB4YBmQQyAwguBjanVaiG33eoMgAPsEHCjBwL844/pNwZAGEEue7CUvby83GIMiu+pp57CVMGG4DYHzwviQ4x2whpFydu2bePxsMoxbRAtEKLcEAaNGzeOo4dDWafTwRBmT+QJEyZA0EJQuTsIYcCxyPuCW8wxZKfBYIAhzJIQwTAHCtPT091WIBYShxfPCJsaT/ktWovVAukCk5yPFRd7L7/88rfffovAgAEDAHEE/hegBwTfCoYFbyh6wjvsJkgabnAgAU+DBG5RBOJAcd+CcMt7zmcIZuz16/SNPuVn3rVbyIN4dbjyeJ6MZ0cYBGiCeLioqAiaC4hE2N0GXg4CeXl57lw8BqBH7xBAYig+HglCwJ0ShDBvhhs3iEFjeHvwCNlBCGMoMFY8AeKpYlYmGga9hjCPRIz7ykvGlQd4PGxH3jDcomR3Y/g6RABXPHLHPyy6F3rQPpC7e+gGxsvdYjxy36KrSIAY3PL0eIT0PAZXnhePQAjzRwhzYwVhEIYGV9wiC08DQgwPIDHC/ApCYh7GI2SBVICTDCgrFIq9e+kzOTzlbXNncRfLgcLDt8cjjMQI8Cw8O8idBlceQAKe5nbiCXh6PEV/eTyIx8PV4Gl4XnSBjwkvE8QHjUOEKmDEsyMeYVxxy688njXwodF9ZI+7iby56ACPdxOe8jQgPHXfood4Sr2vHB1KUTkotxfFI/noIOwu7XZyJ+bl4JYnBvF4SIXIyEh4THzfyD0BPA1Pz0vGU/f1duJNAuERZhEx/JbH33HL6Y6mum8RAKEckDsZz8ID7ltOvO9ITJkZ4dYduD3MAyAkRrGIYZU8JJLl/wetrJqrxdITeAAAAABJRU5ErkJggg==\n" + 
				"\n" + 
				"\"\n" + 
				"                    id=\"p4img1\" style=\"width:100px;height: 80px;padding:10px;\"></td>\n" + 
				"            <td rowspan=\"2\" colspan=\"3\" style=\"border-left:0;width:260px\">\n" + 
				"                <b>Dexter Logistics Pvt. Ltd.</b><br>\n" + 
				"                Office no. 401/402, 4th Floor, Kumar Business Center, Bund Garden Road,\n" + 
				"                Sangamwadi TPS, Pune - 411001\n" + 
				"                www.dexters.co.in<br>\n" + 
				"                CIN No : U63010PN2010PTC136886\n" + 
				"            </td>\n" + 
				"            <td rowspan=\"1\" colspan=\"1\"><b>From</b></td>\n" + 
				"            <td rowspan=\"1\" colspan=\"1\"><b>To</b></td>\n" + 
				"            <td rowspan=\"1\" colspan=\"4\"><b>Way Bill Number</b></td>\n" + 
				"        </tr>\n" + 
				"        <tr colspan=\"11\">\n" + 
				"            <td rowspan=\"1\" colspan=\"1\" style=\"height:70px;text-align: center\">\n" + 
				fromLocation + 
				"            </td>\n" + 
				"            <td rowspan=\"1\" colspan=\"1\" style=\"height:70px;text-align: center\">\n" + 
				toLocation + 
				"            </td>\n" + 
				"            <td rowspan=\"1\" colspan=\"4\" style=\"height:70px;text-align: center\">\n" + 
				"				<img src=\""+barcodeOpfile+"\" id=\"p4img1\" style=\"width:220px;height:60px;\">"
						+ "<span>"+ WayBill_NO+"</span></td>" + 
				"        </tr>\n" + 
				"    </table>\n" + 
				"    <table width=\"700\" border=\"1\" cellpadding=\"3px\" cellspacing=\"0\" align=\"center\"\n" + 
				"        style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px;\">\n" + 
				"        <tr colspan=\"11\" style=\"text-align:center\">\n" + 
				"            <td colspan=\"4\" style=\"width:35%\"><b>Consignor</b></td>\n" + 
				"            <td colspan=\"4\" style=\"width:35%\"><b>Consignee</b></td>\n" + 
				"            <td colspan=\"1\" style=\"border-right:0;width:15%\">Date:</td>\n" + 
				"            <td colspan=\"2\" style=\"border-left:0;text-align:left;width:15%\">\n" + 
				WayBill_Date + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr style=\"height:55px\" rowspan=\"2\" colspan=\"11\">\n" + 
				"            <td colspan=\"4\" rowspan=\"2\"><b>\n" + 
				Consignor_Name + 
				"                </b><br>\n" + 
				Consignor_Address+"<br>Phone No:\n" + 
				Consignor_Phone +"<br>GST No:\n" + consignor_GST_No + 
				"            </td>\n" + 
				"            <td colspan=\"4\" rowspan=\"2\"><b>\n" + Consignee_Name + 
				"                </b><br>\n" + Consignee_Address +"<br>Phone No:\n" + 
				Consignee_Phone+"<br>GST No:\n" + consignee_GST_No + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0;\"><b>Charges</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0;width:80px\"><b>In Rupees</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0;\"><b>Instruction</b></td>\n" + 
				"        </tr>\n" + 
				"        <tr rowspan=\"1\" colspan=\"2\">\n" + 
				"            <td rowspan=\"1\" colspan=\"1\" style=\"border-top:0\">Freight</td>\n" + 
				"            <td rowspan=\"1\" colspan=\"1\" style=\"border-top:0;\">\n" + 
				Freight_Chrs + 
				"            </td>\n" + 
				"            <td rowspan=\"6\" colspan=\"1\" style=\"border-top:0;\"></td>\n" + 
				"        </tr>\n" + 
				"        <tr colspan=\"10\" rowspan=\"1\">\n" + 
				"            <td colspan=\"4\" rowspan=\"1\">Contact Person:</td>\n" + 
				"            <td colspan=\"4\" rowspan=\"1\">Contact Person:</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">Value Surcharge</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">\n" + fov_charge + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"8\" style=\"text-align:center\"><b>PACKAGE INFORMATION</b></td>\n" + 
				"            <td colspan=\"1\">Waybill Charge</td>\n" + 
				"            <td colspan=\"1\">\n" + docket_charge	 + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\"><b>No. Of Packages</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\"><b>Packaging Type</b></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"2\"><b>Volume</b></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"1\"><b>Said To Contain</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\"><b>Charged Weight</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\" style=\"width:80px\">\n" +Total_weight + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">Handling Charges</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">\n" + loadUnload_charge + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"2\" rowspan=\"2\" style=\"border-bottom:0;text-align: center\">\n" + 
				SaidTOContain + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">ODA Charge</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">\n" + oda_charges + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"1\" rowspan=\"3\">\n" + No_Of_Articles + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"3\"></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"3\"></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\"><b>Actual Weight</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\" style=\"width:80px\">\n" + Actual_Weight + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">Other Charges</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">\n" + otherCharge + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>Owners Risk</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>Carriers Risk</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">Fuel Surcharge</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>PAID</b></td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0\"></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0\"></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"1\"><b>MODE OF DISPATCH</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>TOTAL</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\" style=\"border-bottom:0\"><b>\n" + totalCharges+ 
				"                </b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b></b></td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"4\" rowspan=\"1\" style=\"text-align:center\"><b>Invoice No.</b></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"1\" style=\"text-align:center\"><b>VALUE</b></td>\n" + 
				"            <td colspan=\"2\" rowspan=\"3\" style=\"text-align:center;border-bottom:0\">\n" + 
				"                <div style=\"line-height:1.6\">\n" + 
				"                    SURFACE &emsp; <input type=\"text\" style=\"width:25px;height:10px\" /><br>\n" + 
				"                    AIR &emsp;&emsp;&emsp;&emsp; <input type=\"text\" style=\"width:25px;height:10px\" /><br>\n" + 
				"                    RAIL &emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type=\"text\"\n" + 
				"                        style=\"width:25px;height:10px\" /><br>\n" + 
				"                    FTL &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp; <input type=\"text\" style=\"width:25px;height:10px\" />\n" + 
				"                </div>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>SGST/UGST</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">0.00</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>TO PAY</b></td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"4\" rowspan=\"2\">\n" + InvoiceNo + 
				"            </td>\n" + 
				"            <td colspan=\"2\" rowspan=\"2\">\n" + DeclaredValue + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>CGST</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">0.00</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b></b></td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>IGST</b></td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\">0.00</td>\n" + 
				"            <td colspan=\"1\" rowspan=\"1\"><b>CREDIT-CODE</b></td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td colspan=\"4\" rowspan=\"1\" style=\"text-align:center\">\n" + 
				"                <b>For Use At the Time of Delivery Only</b>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"2\" rowspan=\"1\">\n" + 
				"                <b>Declaration</b>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"2\" rowspan=\"1\">\n" + 
				"                <b>GST Paid By:</b>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\">\n" + 
				"                <b>Grand Total</b>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\">\n" + 
				"                <b>\n" + totalCharges + 
				"                </b>\n" + 
				"            </td>\n" + 
				"            <td colspan=\"1\" rowspan=\"2\">\n" + 
				"\n" + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td rowspan=\"1\" colspan=\"2\">\n" + 
				"                NAME\n" + 
				"            </td>\n" + 
				"            <td rowspan=\"5\" colspan=\"2\">\n" + 
				"                STAMP TEL. NO.\n" + 
				"            </td>\n" + 
				"            <td rowspan=\"5\" colspan=\"2\" style=\"width:100px;font-size: 11px\">\n" + 
				"                I/We hereby agree the terms & conditions set out on the reverse of the consignor's copy and declare that\n" + 
				"                the contents on the waybill are true and correct . The To-Pay freight has my/our consent and will be\n" + 
				"                paid by the consignee along with the application service charges at the time of delivery.\n" + 
				"                <br><br>\n" + 
				"                SIGN\n" + 
				"            </td>\n" + 
				"            <td rowspan=\"3\" colspan=\"2\">\n" + 
				"                <div style=\"line-height:1.6\">\n" + 
				"                    &emsp;&emsp;<input type=\"text\" style=\"width:25px;height:10px\" />&emsp;&emsp; Consignor<br>\n" + 
				"                    &emsp;&emsp;<input type=\"text\" style=\"width:25px;height:10px\" />&emsp;&emsp; Consignee<br>\n" + 
				"                    &emsp;&emsp;<input type=\"text\" style=\"width:25px;height:10px\" />&emsp;&emsp; Transporter<br>\n" + 
				"                </div>\n" + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td rowspan=\"1\" colspan=\"2\">SIGNATURE</td>\n" + 
				"            <td rowspan=\"4\" colspan=\"3\" style=\"text-align:center\">POD Copy</td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td rowspan=\"1\" colspan=\"2\">DATE</td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td rowspan=\"1\" colspan=\"2\">REMARK</td>\n" + 
				"            <td rowspan=\"2\" colspan=\"2\">\n" + 
				"                <div style=\"line-height:1.4;font-size:12px\">NAME<br><br>PICKUP DATE:......./....../......<br>PICKUP\n" + 
				"                    TIME:................</div>\n" + 
				"            </td>\n" + 
				"        </tr>\n" + 
				"        <tr>\n" + 
				"            <td rowspan=\"1\" colspan=\"2\"><br></td>\n" + 
				"        </tr>\n" + 
				"    </table>\n" + 
				"</body>\n" + 
				"\n" + 
				"</html>";
		
		return waybillHtml;
	}
	
	
	
	
	 public File genrateBarcode(String waybillno) throws BarcodeException, OutputException, IOException {
	        final BufferedImage barbecue = barbecue(waybillno);

	        System.out.println("Writing image" + waybillno +barbecue);

	       File barcodefile = writeImage(barbecue, waybillno);
	        
	        return  barcodefile;
	    }

	    public static File writeImage(BufferedImage bufferedImage, String barcodeText) throws IOException {
	    	Date date = new Date();
	    	String temp = barcodeText.replaceAll("/", "_");
	    	String url = "/home/senkathir/Desktop/" + temp;
	        File outputfile = new File(MessageFormat.format("{0}.png", url));
	        
	        ImageIO.write(bufferedImage, "png", outputfile);
	        
	       return outputfile;
	    }

	    public static BufferedImage barbecue(String barcodeText) throws BarcodeException, OutputException {
	        Barcode barcode = BarcodeFactory.createCode128(barcodeText);
	        barcode.setBarHeight(100);
	        barcode.setResolution(200);

	        Font font = new Font("Monospaced", Font.PLAIN, 20);
	        barcode.setFont(font);

	        return BarcodeImageHandler.getImage(barcode);
	    }

}
