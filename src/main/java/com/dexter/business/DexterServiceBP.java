package com.dexter.business;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dexter.model.AdminDashboard;
import com.dexter.repository.DexterServiceRepository;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.io.codec.Base64.OutputStream;
import com.dexter.business.GeneratePdf;


@Service
public class DexterServiceBP {
	@Autowired
	private DexterServiceRepository hsServiceRepository;
	@Autowired
	private GeneratePdf generatePdf;
	

	public int checkUser(String user, String password) {
		int resp = 0;

		int check1 = hsServiceRepository.getUserByName(user);
		System.out.println("check1.. " + check1);
		if (check1 == 0) {
			resp = 1;

		} else {

			System.out.println("user.. " + user);
			System.out.println("password.. " + password);
			int check2 = hsServiceRepository.getUserByPass(user, password);
			System.out.println("check2.. " + check2);

			if (check2 == 0) {
				resp = 2;
			}

		}

		return resp;
	}

	public String checkUserDetails(String user) {
		String detail = "";
		System.out.println(".after.. " + user);
		detail = hsServiceRepository.checkUserDetails(user);
		System.out.println(".after.. " + detail);
		return detail;
	}

	public JSONArray getScheduleList(String userid, String desigId) throws Exception {
		String expOccur = "Y";
		String responseMsg = "";
//		String key = getKey();
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			list = hsServiceRepository.getScheduleList(desigId,userid);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("orderId", String.valueOf(obj[0]));
				jsonObject.put("OrderDate", String.valueOf(obj[1]));
				jsonObject.put("LoanProposalID", String.valueOf(obj[2]));
				jsonObject.put("BranchName", String.valueOf(obj[3]));
				jsonObject.put("product", String.valueOf(obj[4]));
				jsonObject.put("ClientName", String.valueOf(obj[5]));
				jsonObject.put("address1", String.valueOf(obj[6]));
				jsonObject.put("address2", String.valueOf(obj[7]));
				jsonObject.put("landMark", String.valueOf(obj[8]));
				jsonObject.put("village", String.valueOf(obj[9]));
				jsonObject.put("district", String.valueOf(obj[10]));
				jsonObject.put("state", String.valueOf(obj[11]));
				jsonObject.put("mobile", String.valueOf(obj[12]));
				jsonObject.put("status", String.valueOf(obj[13]));
				jsonObject.put("scheduleDate", String.valueOf(obj[14]));
				jsonObject.put("scheduleTime", String.valueOf(obj[15]));
				jsonObject.put("stockStatus", String.valueOf(obj[16]));
				loanArray.add(jsonObject);
			}

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;

	}


	public JSONArray getRunSheetDetails(String userId, String desigId) throws Exception {
		String expOccur = "Y";
		String responseMsg = "";
//		String key = getKey();
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getRunSheetDetails(userId);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("runsheetId", String.valueOf(obj[0]));
				jsonObject.put("runSheetNumber", String.valueOf(obj[0]));
				jsonObject.put("driverMobile", String.valueOf(obj[1]));
				loanArray.add(jsonObject);

				
			}
			
			System.out.println("loanArry==="+loanArray);

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	public JSONArray getOrderlist(String userId, String desigId) throws Exception {
		String expOccur = "Y";
		String responseMsg = "";
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			
			list = hsServiceRepository.getOrderlist(userId);
			Iterator<Object> itr = list.iterator();
			
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("requestId", String.valueOf(obj[0]));
				jsonObject.put("vehicle_type", String.valueOf(obj[1]));
				jsonObject.put("reg_no", String.valueOf(obj[2]));
				jsonObject.put("customer_name", String.valueOf(obj[3]));
				jsonObject.put("cust_id", String.valueOf(obj[4]));
				jsonObject.put("driver_name", String.valueOf(obj[6]));
				jsonObject.put("way_bill_type", String.valueOf(obj[14]));
				jsonObject.put("CFT_factor", String.valueOf(obj[15]));
				jsonObject.put("prerequest_code", String.valueOf(obj[16]));
				loanArray.add(jsonObject);

				
			}
			
//			System.out.println("loanArry==="+loanArray);

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	
	
	
	
	
	public JSONArray getWaybillDetails(String userId, String runsheetId) throws Exception {
		String expOccur = "Y";
		String responseMsg = "";
//		String key = getKey();
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getWaybillDetails(runsheetId);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("waybillId", String.valueOf(obj[0]));
				jsonObject.put("waybillNumber", String.valueOf(obj[1]));
				jsonObject.put("customerName", String.valueOf(obj[2]));
				jsonObject.put("consignorName", String.valueOf(obj[3]));
				jsonObject.put("consignorAddress", String.valueOf(obj[4]));
				jsonObject.put("consignorPhone", String.valueOf(obj[5]));
				jsonObject.put("consigneeName", String.valueOf(obj[6]));
				jsonObject.put("consigneeAddress", String.valueOf(obj[7]));
				jsonObject.put("consigneePhone", String.valueOf(obj[8]));
				jsonObject.put("noofArticle", String.valueOf(obj[9]));
				jsonObject.put("totalWeight", String.valueOf(obj[10]));
				
				loanArray.add(jsonObject);

				
			}
			
			System.out.println("loanArry==="+loanArray);

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	
	
	
	public JSONArray updatePod(String waybillId, String userId, JSONArray podImage) throws Exception {
JSONArray loanArray = new JSONArray();
int upd = 0;
int waybillStatusUpd =0;
int updateDetailStatus=0;
try {
	
//	S3Helper help = new S3Helper();
	
	
		if (podImage != null && !podImage.isEmpty()) {
						
//        	String url = "/var/lib/tomcat8/images/";
			String url = "D:\\BVM\\Images\\";
//	        String url = "D:\\HS\\Images\\";
			String fileName = "";
			int id = 0;
			int cnt = 0;
			int insertImagePath = 0;
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	
			
			
			for (int i = 0; i < podImage.size(); i++) {
				++cnt;
				JSONObject jsonobj = (JSONObject) podImage.get(i);
				String base64String = (String) jsonobj.get("image");
				byte[] base64EncodedByteArray = base64String.getBytes();
				byte[] decodedBytes = Base64.decodeBase64(base64EncodedByteArray);
				
				FileOutputStream imageOutFile = new FileOutputStream(
						url + "WaybillPOd" + waybillId + "_" + cnt + ".jpg");
				imageOutFile.write(decodedBytes);
				imageOutFile.close();
				fileName = "WaybillPOd" + waybillId + "_" + cnt + ".jpg";

				File file = new File(url + "WaybillPOd" + waybillId + "_" + cnt + ".jpg");
//		            System.out.println("file = " + file);
				FileInputStream fis = new FileInputStream(file);
//		            System.out.println("fis = " + fis);
				byte[] podFile = new byte[(int) file.length()];
//		            System.out.println("podFile = " + podFile);
				fis.read(podFile);
				fis.close();
				insertImagePath = hsServiceRepository.updatePod(waybillId, fileName,
						userId, podFile);
			}
			System.out.println("insertImagePath==="+insertImagePath);
			if(insertImagePath > 0) {
				waybillStatusUpd = hsServiceRepository.updateWaybillStatus(waybillId);
			}
			
			if(waybillStatusUpd > 0) {
				updateDetailStatus = hsServiceRepository.updateDetailStatus(waybillId);
			}
			
			
			if (updateDetailStatus > 0) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "1");
				loanArray.add(jsonObject);
			} else {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "0");
				loanArray.add(jsonObject);
			}
			
			
			
		}

	

} catch (Exception ex) {
	ex.printStackTrace();
	
	JSONObject jsonObject = new JSONObject();
	jsonObject.put("status", "0");
	loanArray.add(jsonObject);
}

return loanArray;
}

	public JSONArray getManualnolist(String BranchId) throws Exception {
		String expOccur = "Y";
		String responseMsg = "";
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getManualnolist(BranchId);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
//				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("PreNo", itr.next());
				
				loanArray.add(jsonObject);

				
			}
			
			System.out.println("Mannual no array==="+loanArray);

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	
	public JSONArray getSKUNamelist(String BranchId) throws Exception {
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getSKUNamelist(BranchId);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("Article_ID", String.valueOf(obj[0]));
				jsonObject.put("SKU_Name", String.valueOf(obj[1]));
				
				loanArray.add(jsonObject);

				
			}

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	public JSONArray getPakaginglist() throws Exception {
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getPakaginglist();
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("Pkg_Id", String.valueOf(obj[0]));
				jsonObject.put("PKG_value", String.valueOf(obj[1]));
				
				loanArray.add(jsonObject);

				
			}

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	public JSONArray getArticlelist(String waybillId) throws Exception {
		JSONArray loanArray = new JSONArray();
		try {
			List<Object> list = new ArrayList<Object>();
			
			list = hsServiceRepository.getArticlelist(waybillId);
			Iterator<Object> itr = list.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("Article_Name", String.valueOf(obj[0]));
				jsonObject.put("PKG_Name", String.valueOf(obj[1]));
				jsonObject.put("NoOfArticle", String.valueOf(obj[2]));
				jsonObject.put("SaidTOContain", String.valueOf(obj[3]));
				jsonObject.put("Length", String.valueOf(obj[4]));
				jsonObject.put("Breadth", String.valueOf(obj[5]));
				jsonObject.put("Height", String.valueOf(obj[6]));
				jsonObject.put("Uom_Type_Id", String.valueOf(obj[7]));
				jsonObject.put("Weight_Type_Id", String.valueOf(obj[8]));
				jsonObject.put("WayBill_Article_Id", String.valueOf(obj[9]));
				jsonObject.put("ewaybill_id", String.valueOf(obj[10]));
				jsonObject.put("saidToContain_Id", String.valueOf(obj[11]));
				jsonObject.put("noOfSaidToContains", String.valueOf(obj[12]));
				jsonObject.put("per_piece_weight", String.valueOf(obj[13]));
				jsonObject.put("Volumetricweight", String.valueOf(obj[14]));
				jsonObject.put("WayBill_Mode", String.valueOf(obj[15]));
				
				
				loanArray.add(jsonObject);

				
			}

		} catch (Exception ex) {
			System.out.println("Exception===" + ex.getMessage());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", "0");
			loanArray.add(jsonObject);
		}

		return loanArray;
	}
	
	
	public int Insertarticle(String waybillId,String userId,String SKU_Id,String SaidtoContianId,String nospkg,
								String nossku,String Length,String Breadth,String Height,Long uomType,Long weight_type,
								String per_piece_weight,String WayBill_Mode,String RateType,String volumetricweight) {
		int status = 0;
		
		
		try {
		
		status = hsServiceRepository.Insertarticles(waybillId,
													userId,
													SKU_Id,
													SaidtoContianId,
													nospkg,
													nossku,
													Length,
													Breadth,
													Height,
													uomType,
													weight_type,
													per_piece_weight,
													WayBill_Mode,
													RateType,
													volumetricweight);
						}catch(Exception e){
				
						System.out.println("Exception" + e.getMessage() );
						
						}
						
						return status;
					}
					
	
//						public int confirmArticles(String waybillId,String userId) {
//								int status = 0;
//								
//								
//								try {
//								
//								status = hsServiceRepository.confirmArticles(waybillId,userId);
//								}catch(Exception e){
//								
//								System.out.println("Exception" + e.getMessage() );
//								
//								}
//								
//								return status;
//					}

						
						public int Insertewaybilllist(String ewaybill_id,String ewaybill_no,String ewaybill_date,String ewaybill_expiry_date,String invoice_no,String invoice_date,String invoice_value,String vehicle_id,String vehicle_no,String status,String post_status,String from_gst_no,String to_gst_no,String from_state,String to_state) {
								int opstatus = 0;
								int confirmstatus = 0;
								
								
								try {
									
									
									String pattern = "yyyy-MM-dd";
									SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

									Date invoice_date1 = simpleDateFormat.parse(invoice_date);
									invoice_date = simpleDateFormat.format(invoice_date1);
									
									System.out.println(invoice_date);
									opstatus = hsServiceRepository.Insertewaybilllist(ewaybill_id, ewaybill_no, 
																ewaybill_date, ewaybill_expiry_date, 
																invoice_no, invoice_date, 
																invoice_value, vehicle_id, 
																vehicle_no, status, 
																post_status, from_gst_no, 
																to_gst_no, from_state, to_state);
									System.out.println("Status of the ewaybill insert Query == > " + opstatus);
									
									if(opstatus > 0) {
										
											
										JSONArray loanArray = new JSONArray();
										
											List<Object> list = new ArrayList<Object>();
											
											list = hsServiceRepository.getapiarticleId();
											
											
											Iterator<Object> itr = list.iterator();
											
											int api_articleId =0;
											while (itr.hasNext()) {
												
												 api_articleId = (Integer) itr.next();
											}
											System.out.println("article Id ==>"+api_articleId);
											if(api_articleId != 0) {
												
												
													confirmstatus = hsServiceRepository.confirmArticles(api_articleId);
													
											}
										}
												}catch(Exception e){
													e.printStackTrace();

												System.out.println("Exception ==> " + e.getMessage() );
												
												}
												
												return confirmstatus;
											}
						

						public int Deletearticles(String waybillId) {
								int status = 0;
								try {
								
								status = hsServiceRepository.Deletearticles(waybillId);
								}catch(Exception e){
								
								System.out.println("Exception" + e.getMessage() );
								
								}
								
								return status;
								}

						public JSONArray GetArticleStatus(String waybillnum) throws Exception {
							JSONArray loanArray = new JSONArray();
							try {
								List<Object> list = new ArrayList<Object>();
								
								list = hsServiceRepository.GetArticleStatus(waybillnum);
								Iterator<Object> itr = list.iterator();
								while (itr.hasNext()) {
									JSONObject jsonObject = new JSONObject();
									
									jsonObject.put("Article_Staus", itr.next());									
									
									loanArray.add(jsonObject);

									
								}

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
	
						public int Insertoperationwaybill(	String ewaybill_no,
								String ewaybillId,
								String userId,
			                    String invoice_no,
			                    String ewaybill_date,
			                    long waybilltype,
			                    String customername,
			                    String Cust_id,
			                    String Consignor_Name,
			                    String ConsigneeName,
			                    String Actualweight,
			                    String ewaybill_expiry_date,
			                    long movmenttype,
			                    long weighttype,
			                    String RequestID,
			                    String appoinment,
			                    String appointment_date_time,
			                    String floor_applicable,
			                    String no_of_floors,
			                    long coddod_applicable,
			                    long specialratetype,
			                    String mannualnum,long barcodeType) {
		int status = 0;
		if(appointment_date_time != null) {
		appointment_date_time = appointment_date_time.replaceAll("/", "-");
		String date = appointment_date_time.split(" ")[0];
		String time = appointment_date_time.split(" ")[1];
		String[] temp = date.split("-");
		if(temp[2].length()==4) {
			if(temp[0].toString().length()!=2) {
				temp[0] = "0"+temp[0];
			}
			if(temp[1].toString().length()!=2) {
				temp[1] = "0"+temp[1];
			}
			String tmp = temp[0];
			temp[0] = temp[2];
			temp[2]=tmp;
		}else {
			if(temp[2].toString().length()!=2) {
				temp[2] = "0"+temp[2];
			}
			if(temp[1].toString().length()!=2) {
				temp[1] = "0"+temp[1];
			}
		}
		appointment_date_time = temp[0]+"-"+temp[1]+"-"+temp[2]+" "+time;
		}
		try {
		status = hsServiceRepository.Insertoperationwaybill(
				ewaybillId,userId,
                ewaybill_date,
                waybilltype,
                customername,
                Cust_id,
                Consignor_Name,
                ConsigneeName,
                Actualweight,
                movmenttype,
                weighttype,
                RequestID,
                appoinment,
                appointment_date_time,
                floor_applicable,
                no_of_floors,
                coddod_applicable,
                specialratetype,
                mannualnum,barcodeType);
					
					if(status > 0) {
						
						
						
						List<Object> list = new ArrayList<Object>();
						
						list = hsServiceRepository.getOperationwaybillId();
						
						
						Iterator<Object> itr = list.iterator();
						
						int api_articleId =0;
						while (itr.hasNext()) {
							
							 api_articleId = (Integer) itr.next();
						}
						System.out.println("article Id ==>"+api_articleId);
						if(api_articleId != 0) {
							
									int updatestatus1 = hsServiceRepository.updatearticlestatus(api_articleId);
									
									if(updatestatus1 > 1) {
								
										int updatestatus = hsServiceRepository.updatepickuplist(RequestID);
										}
								}
								
							
								
							}
						}catch(Exception e){
				
						System.out.println("Exception" + e.getMessage() );
						
						}
						
						return status;
					}
						
						public JSONArray getTriplist(String userId,String DesigId) throws Exception {
							JSONArray loanArray = new JSONArray();
							try {
								List<Object> list = new ArrayList<Object>();
								
								list = hsServiceRepository.getTriplist(userId);
								Iterator<Object> itr = list.iterator();
								while (itr.hasNext()) {
									Object[] obj = (Object[]) itr.next();
									JSONObject jsonObject = new JSONObject();
									
									jsonObject.put("trip_id",String.valueOf(obj[0]));
									jsonObject.put("trip_code",String.valueOf(obj[1]));
									jsonObject.put("Origin",String.valueOf(obj[2]));
									jsonObject.put("destination",String.valueOf(obj[3]));
									jsonObject.put("ewaybill_no",String.valueOf(obj[4]));
									jsonObject.put("trip_status_id",String.valueOf(obj[5]));
									jsonObject.put("last_km",String.valueOf(obj[6]));
									jsonObject.put("vehicle_origin_actual_reporting_date",String.valueOf(obj[7]));
									jsonObject.put("vehicle_origin_actual_reporting_time",String.valueOf(obj[8]));
									jsonObject.put("vehicle_actual_loading_date",String.valueOf(obj[9]));
									jsonObject.put("vehicle_actual_loading_time",String.valueOf(obj[10]));
									jsonObject.put("trip_actual_start_date",String.valueOf(obj[11]));
									jsonObject.put("trip_actual_start_time",String.valueOf(obj[12]));
									jsonObject.put("vehicle_destination_actual_reporting_date",String.valueOf(obj[13]));
									jsonObject.put("vehicle_destination_actual_reporting_time",String.valueOf(obj[14]));
									jsonObject.put("extra_expense_status",String.valueOf(obj[15]));
									jsonObject.put("vechile_actual_unloading_date",String.valueOf(obj[16]));
									jsonObject.put("vehicle_actual_unloading_time",String.valueOf(obj[17]));
									jsonObject.put("order_allocation",String.valueOf(obj[18]));
									jsonObject.put("substatus",String.valueOf(obj[19]));
									jsonObject.put("custId",String.valueOf(obj[20]));
									jsonObject.put("customerName",String.valueOf(obj[21]));

									jsonObject.put("expOccur",'N');
									
									
									
									loanArray.add(jsonObject);

									
								}

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						public JSONArray updateReportingDetails(String tripId,String subStatusId,String reportingDate,String reportingTime) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateReportingDetails(tripId,subStatusId);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "N");
										jsonObject.put("status", "Succesfully updated");
										loanArray.add(jsonObject);
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						
						public JSONArray updateLoadingDetails(String tripId,String subStatusId,String reportingDate,String reportingTime) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateLoadingDetails(tripId,subStatusId);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "N");
										jsonObject.put("status", "Succesfully updated");
										loanArray.add(jsonObject);
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						
						
						public JSONArray updateGateoutDetails(String tripId,String subStatusId,String reportingDate,String reportingTime,String odoMeterReading) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateGateoutDetails(tripId,subStatusId,odoMeterReading);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										
										
										String get_trip_plan_date_time = hsServiceRepository.get_trip_plan_date_time(tripId);
										System.out.println("get_trip_plan_date_time====="+get_trip_plan_date_time);
										String[] temp = get_trip_plan_date_time.split("~");
							            String tripPlanEndDate = "";
							            String tripPlanEndTime = "";
							            tripPlanEndDate = temp[0];
							            tripPlanEndTime = temp[1];
							            System.out.println("tripPlanEndDate:" + tripPlanEndDate);
							            System.out.println("tripPlanEndTime:" + tripPlanEndTime);
							            
//							            Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(tripPlanEndDate);
//							            DateFormat date=new SimpleDateFormat("yyyy-MM-dd");
//							            Date date1= date.parse(tripPlanEndDate); 
//							            System.out.println("date==="+date1);
//							            String trip_plan = date1.toString();
//							            System.out.println("aftre trip_plan==="+trip_plan);
							            
							            
							            
							            
										
										
										
										
										
										String statusId = "10";
										int upd_trip_status_id = hsServiceRepository.updateTripStatus(tripId,statusId,tripPlanEndDate,tripPlanEndTime);
										if(upd_trip_status_id > 0 ) {
											
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("expOccur", "N");
											jsonObject.put("status", "Succesfully updated");
											loanArray.add(jsonObject);	
										}else {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("expOccur", "Y");
											jsonObject.put("status", "Update Failed in updating trip status");
											loanArray.add(jsonObject);
										}
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed in trip sub status");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", ex);
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						public JSONArray updateDestinationReachedDetails(String tripId,String reportingDate,String reportingTime) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateDestinationReachedDetails(tripId);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "N");
										jsonObject.put("status", "Succesfully updated");
										loanArray.add(jsonObject);
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						public JSONArray updateUnloadingDetails(String tripId,String reportingDate,String reportingTime) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateUnloadingDetails(tripId);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "N");
										jsonObject.put("status", "Succesfully updated");
										loanArray.add(jsonObject);
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						
						public JSONArray updateTripEnd(String tripId,String reportingDate,String reportingTime) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
								String statusId = "12";
									int upd_trip_status = hsServiceRepository.updateTripEnd(tripId,statusId,reportingDate,reportingTime);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "N");
										jsonObject.put("status", "Succesfully updated");
										loanArray.add(jsonObject);
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						
						public JSONArray updateDocument(String tripId, JSONArray podImage, String userId) throws Exception {
							JSONArray loanArray = new JSONArray();

							try {

								if (podImage != null && !podImage.isEmpty()) {

							        String url = "/var/lib/tomcat8/webapps/throttle/content/upload";
//									String url = "/var/lib/tomcat8/uploadFiles/fuel/";
//						            String url = "D:\\HS\\Images\\";
//									 String url = "D:\\BVM\\Images\\";
									 
									String fileName = "";
									String filePath = "";
									int id = 0;
									int cnt = 0;
									int insertImagePath = 0;
									String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
									
//									int upd = bvmServiceRepository.updateFuelFillingStatus(odometerReadings, filledLtrs, selectedBunk,
//											selectedVehicle, userId);
//									id = bvmServiceRepository.getLastInsertId();
//									System.out.println("getLastInsertId.. " + id);
					//
////								            map.put("podFile", podFile);
									
									
									for (int i = 0; i < podImage.size(); i++) {
										++cnt;
										JSONObject jsonobj = (JSONObject) podImage.get(i);
										String base64String = (String) jsonobj.get("image");
										byte[] base64EncodedByteArray = base64String.getBytes();
										byte[] decodedBytes = Base64.decodeBase64(base64EncodedByteArray);
										FileOutputStream imageOutFile = new FileOutputStream(
												url + "TripDocument" + tripId + "_" + cnt + ".jpg");
										imageOutFile.write(decodedBytes);
										imageOutFile.close();
										fileName = "TripDocument" + tripId + "_" + cnt + ".jpg";
										filePath = url + "TripDocument" + tripId + "_" + cnt + ".jpg";

										File file = new File(url + "TripDocument" + tripId + "_" + cnt + ".jpg");
//								            System.out.println("file = " + file);
										FileInputStream fis = new FileInputStream(file);
//								            System.out.println("fis = " + fis);
										byte[] podFile = new byte[(int) file.length()];
//								            System.out.println("podFile = " + podFile);
										fis.read(podFile);
										fis.close();
										insertImagePath = hsServiceRepository.insertDocumentImagePath(tripId, fileName,
												userId, filePath);
									}
									System.out.println("insertImagePath===="+insertImagePath);
									String subStatusId = "3";
//									int own_hire = hsServiceRepository.own_hire(tripId);
//									System.out.println("own_hire===="+own_hire);
//									if(own_hire == 1) {
//										subStatusId = "3";	
//									}else {
//										subStatusId = "4";
//									}
									if(subStatusId !=null) {
										int upd_trip_status = hsServiceRepository.updateDocumentDetails(tripId,subStatusId);
										
										if (upd_trip_status > 0) {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("status", "1");
											loanArray.add(jsonObject);
										} else {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("status", "0");
											loanArray.add(jsonObject);
										}
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("status", "0");
										loanArray.add(jsonObject);
									}
								
								}

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						public JSONArray updateTripPod(String tripId, JSONArray podImage, String userId) throws Exception {
							JSONArray loanArray = new JSONArray();

							try {

								if (podImage != null && !podImage.isEmpty()) {

//							        String url = "/var/lib/tomcat8/webapps/throttle/content/upload";
//									String url = "/var/lib/tomcat8/uploadFiles/fuel/";
//						            String url = "D:\\HS\\Images\\";
									 String url = "D:\\BVM\\Images\\";
									String fileName = "";
									int id = 0;
									int cnt = 0;
									int insertImagePath = 0;
									String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
									
//									int upd = bvmServiceRepository.updateFuelFillingStatus(odometerReadings, filledLtrs, selectedBunk,
//											selectedVehicle, userId);
//									id = bvmServiceRepository.getLastInsertId();
//									System.out.println("getLastInsertId.. " + id);
					//
////								            map.put("podFile", podFile);
									
									
									for (int i = 0; i < podImage.size(); i++) {
										++cnt;
										JSONObject jsonobj = (JSONObject) podImage.get(i);
										String base64String = (String) jsonobj.get("image");
										byte[] base64EncodedByteArray = base64String.getBytes();
										byte[] decodedBytes = Base64.decodeBase64(base64EncodedByteArray);
										FileOutputStream imageOutFile = new FileOutputStream(
												url + "TripPod" + tripId + "_" + cnt + ".jpg");
										imageOutFile.write(decodedBytes);
										imageOutFile.close();
										fileName = "TripPod" + tripId + "_" + cnt + ".jpg";

										File file = new File(url + "TripPod" + tripId + "_" + cnt + ".jpg");
//								            System.out.println("file = " + file);
										FileInputStream fis = new FileInputStream(file);
//								            System.out.println("fis = " + fis);
										byte[] podFile = new byte[(int) file.length()];
//								            System.out.println("podFile = " + podFile);
										fis.read(podFile);
										fis.close();
										insertImagePath = hsServiceRepository.insertPodImagePath(tripId, fileName,
												userId, podFile);
									}
									String subStatusId = "7";
									int upd_trip_status = hsServiceRepository.updateDocumentDetails(tripId,subStatusId);

									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("status", "1");
										loanArray.add(jsonObject);
									} else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("status", "0");
										loanArray.add(jsonObject);
									}

								}

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						public JSONArray getInterimTripList(String tripId) throws Exception {
							String expOccur = "Y";
							String responseMsg = "";
//							String key = getKey();
							JSONArray loanArray = new JSONArray();

							try {
								int i = 0;
								List<Object> list = new ArrayList<Object>();
								list = hsServiceRepository.getInterimTripList(tripId);
//								list.size();
								System.out.println("list size====" + list.size());
								if (list.size() > 0) {
									expOccur = "N";
									System.out.println("list size greater than 0");
									
									Iterator<Object> itr = list.iterator();
									while (itr.hasNext()) {
										System.out.println();
										Object[] obj = (Object[]) itr.next();
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("pointId", String.valueOf(obj[0]));
										jsonObject.put("pointName", String.valueOf(obj[1]));
										jsonObject.put("inDate", String.valueOf(obj[2]));
										jsonObject.put("inTime", String.valueOf(obj[3]));
										jsonObject.put("outDate", String.valueOf(obj[4]));
										jsonObject.put("outTime", String.valueOf(obj[5]));
										jsonObject.put("fileName", String.valueOf(obj[6]));
										jsonObject.put("completedFlag", String.valueOf(obj[7]));
										jsonObject.put("routeOrder", String.valueOf(obj[8]));
										
										jsonObject.put("expOccur", expOccur);
										loanArray.add(jsonObject);

									}
									
									
								} else {
									System.out.println("list size lesser than 0");
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("expOccur", "No Trips");
									loanArray.add(jsonObject);
								}

								

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								ex.printStackTrace();
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								jsonObject.put("expOccur", expOccur);
								loanArray.add(jsonObject);

							}

							return loanArray;

						}
						
						
						
						public JSONArray updateInterimGateInReportingDetails(String tripId,String pointId,String reportingDate,String reportingTime,String routeOrder) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateInterimGateInReportingDetails(tripId,pointId,routeOrder);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("expOccur", "N");
											jsonObject.put("status", "Succesfully updated");
											loanArray.add(jsonObject);	
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed in trip sub status");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", ex);
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						public JSONArray updateInterimGateOutReportingDetails(String tripId,String pointId,String reportingDate,String reportingTime,String routeOrder) throws Exception {

							JSONArray loanArray = new JSONArray();

							try {
									int upd_trip_status = hsServiceRepository.updateInterimGateOutReportingDetails(tripId,pointId,routeOrder);

									System.out.println("upd_trip_status==" + upd_trip_status);
									if (upd_trip_status > 0) {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("expOccur", "N");
											jsonObject.put("status", "Succesfully updated");
											loanArray.add(jsonObject);	
									}else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("expOccur", "Y");
										jsonObject.put("status", "Update Failed in trip sub status");
										loanArray.add(jsonObject);
									}
							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("expOccur", "Y");
								jsonObject.put("status", ex);
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						
						
						public JSONArray uploadInterimPod(String tripId, JSONArray podImage, String userId,String pointId,String routeOrder) throws Exception {
							JSONArray loanArray = new JSONArray();

							try {

								if (podImage != null && !podImage.isEmpty()) {

//							        String url = "/var/lib/tomcat8/webapps/throttle/content/upload";
//									String url = "/var/lib/tomcat8/uploadFiles/fuel/";
//						            String url = "D:\\HS\\Images\\";
									 String url = "D:\\BVM\\Images\\";
									String fileName = "";
									int id = 0;
									int cnt = 0;
									int insertImagePath = 0;
									String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
									
//									int upd = bvmServiceRepository.updateFuelFillingStatus(odometerReadings, filledLtrs, selectedBunk,
//											selectedVehicle, userId);
//									id = bvmServiceRepository.getLastInsertId();
//									System.out.println("getLastInsertId.. " + id);
					//
////								            map.put("podFile", podFile);
									
									
									for (int i = 0; i < podImage.size(); i++) {
										++cnt;
										JSONObject jsonobj = (JSONObject) podImage.get(i);
										String base64String = (String) jsonobj.get("Image1");
										byte[] base64EncodedByteArray = base64String.getBytes();
										byte[] decodedBytes = Base64.decodeBase64(base64EncodedByteArray);
										FileOutputStream imageOutFile = new FileOutputStream(
												url + "TripPod" + tripId +"_"+ pointId+".jpg");
										imageOutFile.write(decodedBytes);
										imageOutFile.close();
										fileName = "TripPod" + tripId + "_" + pointId + ".jpg";

										File file = new File(url + "TripPod" + tripId + "_" + pointId + ".jpg");
//								            System.out.println("file = " + file);
										FileInputStream fis = new FileInputStream(file);
//								            System.out.println("fis = " + fis);
										byte[] podFile = new byte[(int) file.length()];
//								            System.out.println("podFile = " + podFile);
										fis.read(podFile);
										fis.close();
										insertImagePath = hsServiceRepository.insertPodImagePath(tripId, fileName,
												userId, podFile);
									}
									
									int upd_trip_status = hsServiceRepository.updateConsignmentInvoiceDetails(tripId,pointId,fileName,routeOrder);

									if (upd_trip_status > 0) {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("status", "1");
										loanArray.add(jsonObject);
									} else {
										JSONObject jsonObject = new JSONObject();
										jsonObject.put("status", "0");
										loanArray.add(jsonObject);
									}

								}

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						
						

						public JSONArray getWaybillPdf(String waybillId) throws Exception {
							String expOccur = "Y";
							String responseMsg = "";
							JSONArray loanArray = new JSONArray();
							try {
								
								
								Date date = new Date();
								
								long CurrentTime = date.getTime();
//								String url = "/var/lib/tomcat8/webapps/throttle/content/upload";
								String url = "/home/senkathir/Desktop/";
								
								String waybillPrintHtml = generatePdf.Waybillprint(waybillId);
								
								String FileName =  "WaybillPrint_"+CurrentTime +".pdf";
								FileOutputStream fileOutputStream = new FileOutputStream(url+FileName);
								 HtmlConverter.convertToPdf(waybillPrintHtml,fileOutputStream);
								
								System.out.println("loanArry==="+loanArray);
								
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "Pdf Generated Sucessfully");
								loanArray.add(jsonObject);

							} catch (Exception ex) {
								System.out.println("Exception===" + ex.getMessage());
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("status", "0");
								loanArray.add(jsonObject);
							}

							return loanArray;
						}
						

}