
package com.dexter.controller;

import java.io.File;

import org.json.simple.JSONArray;



import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dexter.business.CryptoLibrary;
import com.dexter.business.DexterServiceBP;
import com.dexter.repository.DexterServiceRepository;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import com.itextpdf.html2pdf.HtmlConverter;



@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class DexterServiceController {

	@Autowired
	DexterServiceBP hsServiceBP;

	@Autowired
	private DexterServiceRepository hsServiceRepository;
	
	

	@PostMapping(path = "/postOrderDetails", consumes = "application/json", produces = "application/json")
	public JSONArray postOrderDetails(@RequestBody String requestString,
			@RequestParam(value = "authToken") String key) {

		JSONArray responseArray = new JSONArray();
		JSONObject responseobj;

		try {

			String ID = "", ClientUniqueID = "", Pincode = "";

			String MemberOfferPrice = "";
			double loanproposalID = 0;
			String BatchNumber = "", BatchDate = "", BranchID = "", BranchName = "", LoanApprovaldate = "",
					Location = "", ClientID = "", ClientName = "", ProductID = "", Model = "", AddressLine1 = "",
					AddressLine2 = "", Landmark = "", Village = "", DistrictName = "", StateName = "",
					CenterLatLong = "", MobileNumber = "", SpouseName = "", UN1 = "", UN2 = "", UN3 = "";
			String WarehouseID = "", BusinessID = "";
			String Sgst = "", Cgst = "", Igst = "";
			String AlternateMobileNumber = "", TotalPrice = "";
			String responseValue = responseArray.toJSONString();
			int postdump = hsServiceRepository.postOrderDump(requestString, responseValue, "BFIL", "postOrderDetails");
			System.out.println("postOrderDetails postdump=" + postdump);

		} catch (Exception e) {
			System.out.println("cant process this message..........." + e.getMessage());
			// e.printStackTrace();
		}

		return responseArray;
	}

	@GetMapping(path = "/checkLoginUser", produces = "application/json")
	public JSONArray checkLoginUser(@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password) {

		JSONArray returnArray = new JSONArray();

		try {

			CryptoLibrary cl = new CryptoLibrary();
			JSONObject responseobj = new JSONObject();

			password = cl.encrypt(password);

			int check = hsServiceBP.checkUser(username, password);

			if (check == 0) {

				String userDetail = hsServiceBP.checkUserDetails(username);

				if (userDetail != null) {
					String[] userids = userDetail.split("~");
					responseobj.put("UserId", userids[0]);
					responseobj.put("DesigId", userids[1]);
					responseobj.put("BranchId", userids[2]);
					responseobj.put("status", "Success");
				} else {
					responseobj.put("DesigId", 0);
					responseobj.put("UserId", 0);
					responseobj.put("BranchId", 0);
					responseobj.put("status", "Not Exist");
				}

			}

			else if (check == 1) {
				responseobj.put("DesigId", 0);
				responseobj.put("UserId", 0);
				responseobj.put("BranchId", 0);
				responseobj.put("status", "UserName Not Exist");

			} else if (check == 2) {
				responseobj.put("DesigId", 0);
				responseobj.put("UserId", 0);
				responseobj.put("BranchId", 0);
				responseobj.put("status", "Password Not Exist");

			}

			returnArray.add(responseobj);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnArray;
	}

	@GetMapping(path = "/getRunSheetDetails", produces = "application/json")
	public JSONArray getRunSheetDetails(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "desigId") String desigId) {

		JSONArray returnArray = new JSONArray();

		try {
			System.out.println("userId..in getRunSheetDetails== " + userId);
			System.out.println("desigId..in getRunSheetDetails=== " + desigId);
			returnArray = hsServiceBP.getRunSheetDetails(userId, desigId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnArray;
	}

	@GetMapping(path = "/getWaybillDetails", produces = "application/json")
	public JSONArray getWaybillDetails(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "runsheetId") String runsheetId) {

		JSONArray returnArray = new JSONArray();

		try {
			System.out.println("userId..in getWaybillDetails== " + userId);
			System.out.println("runsheetId..in getWaybillDetails=== " + runsheetId);
			returnArray = hsServiceBP.getWaybillDetails(userId, runsheetId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnArray;
	}

	@PostMapping(path = "/updatePod", produces = "application/json", consumes = "application/json")
	public JSONArray updatePod(@RequestParam(value = "waybillId") String waybillId,
			@RequestParam(value = "userId") String userId, @RequestBody String requestString) {

		JSONArray returnArray = new JSONArray();

		try {

			JSONArray jsonobjs = (JSONArray) new JSONParser().parse(requestString);
			JSONObject jsonobj = (JSONObject) jsonobjs.get(0);
			JSONArray innerArry = (JSONArray) jsonobj.get("podImage");
//			System.out.println("innerArry.. " + innerArry);

			returnArray = hsServiceBP.updatePod(waybillId, userId, innerArry);

			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "0");
				jsonObject.put("Remarks", "Data Not Updated");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnArray;
	}

	@GetMapping(path = "/callEwayBillApi", consumes = "application/json", produces = "application/json")
	public JSONArray insertWhatsappApi(@RequestParam(value = "ewayBillNo") String ewayBillNo,
			@RequestParam(value = "gstNo") String gstNo) {
		JSONArray returnArray = new JSONArray();
		try {
			final String initLink = "https://dev.api.easywaybill.in/ezewb/v1/auth/initlogin";
			String initUserName = "arun@entitlesolutions.com";
			String initPassword = "Abcd@1234";
			String initPayload = "{ \n" + "\"userid\":" + " \"" + initUserName + "\",\n" + "\"password\":" + " \""
					+ initPassword + "\"\n" + "}";
			OkHttpClient initClient = new OkHttpClient().newBuilder().build();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody initBody = okhttp3.RequestBody.create(mediaType, initPayload);
			Request initRequest = new Request.Builder().url(initLink).method("POST", initBody)
//                    .addHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzIjoxLCJ1Ijo5NCwiZXhwIjoxNjM0OTg3MzQ5LCJpYXQiOjE2MzQ5NjU3NDksIm4iOiJIYWkgbG9naWNzIiwibyI6NH0.a8IcQePLuFMb_y1yzXtgZ0SvcCDLe-jAw0UGfEr2eEA")
					.addHeader("Content-Type", "application/json").build();
			okhttp3.Response initResponse = initClient.newCall(initRequest).execute();
			String initResponseBody = initResponse.body().string();
			JSONParser tokenParser = new JSONParser();
			JSONObject jsonResponse = (JSONObject) tokenParser.parse(initResponseBody);
			System.out.println("token  " + jsonResponse.get("response").toString());
			JSONObject tokenBody = (JSONObject) tokenParser.parse(jsonResponse.get("response").toString());
			String token = tokenBody.get("token").toString();
			System.out.println("tokne  get  " + token);
			final String completeLogin = "https://dev.api.easywaybill.in/ezewb/v1/auth/completelogin";
			String completePayload = "{\n" + "\"token\":\"" + token + "\",\n\"orgid\":4\n}";
			okhttp3.RequestBody completeBody = okhttp3.RequestBody.create(mediaType, completePayload);
			Request completeRequest = new Request.Builder().url(completeLogin).method("POST", completeBody)
					.addHeader("Content-Type", "application/json").build();
			okhttp3.Response completeResponse = initClient.newCall(completeRequest).execute();
			String completeResponseBody = completeResponse.body().string();
			JSONObject completeResponseJson = (JSONObject) tokenParser.parse(completeResponseBody);
			JSONObject bearerBody = (JSONObject) tokenParser.parse(completeResponseJson.get("response").toString());
			String bearerToken = bearerBody.get("token").toString();
			System.out.println("bearertoken  " + bearerToken);
			final String getResult = "https://dev.api.easywaybill.in/ezewb/v1/ewb/data?ewbNo=" + ewayBillNo + "&gstin="
					+ gstNo;
			Request resultRequest = new Request.Builder().url(getResult).method("GET", null)
					.addHeader("Authorization", "Bearer " + bearerToken).addHeader("Content-Type", "application/json")
					.build();
			okhttp3.Response resultResponse = initClient.newCall(resultRequest).execute();
			String finalResult = resultResponse.body().string();
			JSONObject finalResponseJson = (JSONObject) tokenParser.parse(finalResult);
			String status = finalResponseJson.get("status").toString();
			if ("1".equals(status)) {
				JSONObject finalBody = (JSONObject) tokenParser.parse(finalResponseJson.get("response").toString());
				JSONArray itemList = (JSONArray) tokenParser.parse(finalBody.get("itemList").toString());
				System.out.println("itemList Size  " + itemList.size());
				System.out.println("result  " + finalResult);
				String itemSize = itemList.size() + "";
				String invoiceNo = finalBody.get("docNo").toString();
				String invoiceValue = finalBody.get("totInvValue").toString();
				String ewayBillId = finalBody.get("ewbId").toString();
				String ewayBillDate = finalBody.get("ewbDate").toString();
				String ewayBillExpiry = finalBody.get("validUpto").toString();
				String ewayBillUpdatedDate = "";
				if (finalBody.get("ewbUpdateDate") == null || "".equals(finalBody.get("ewbUpdateDate"))) {
					ewayBillUpdatedDate = ewayBillDate;
				} else {
					ewayBillUpdatedDate = finalBody.get("ewbUpdateDate").toString();
				}
				String invoiceDate = finalBody.get("docDate").toString();
				String consignorGst = finalBody.get("fromGstin").toString();
				String consignorName = finalBody.get("fromTrdName").toString();
				String consignorAddress = finalBody.get("fromAddr1").toString();
				String consignorPlace = finalBody.get("fromPlace").toString();
				String consignorPincode = finalBody.get("fromPincode").toString();
				String consignorState = finalBody.get("fromStateCode").toString();
				String consigneeGst = finalBody.get("toGstin").toString();
				String consigneeName = finalBody.get("toTrdName").toString();
				String consigneeAddress = finalBody.get("toAddr1").toString();
				String consigneePlace = finalBody.get("toPlace").toString();
				String consigneePincode = finalBody.get("toPincode").toString();
				String consigneeState = finalBody.get("toStateCode").toString();
				String transId = finalBody.get("transId").toString();
				String transName = finalBody.get("transName").toString();
				String transDocNo = finalBody.get("transDocNo").toString();
				String transDocDate = finalBody.get("transDocDate").toString();
				String vehicleNo = "";
				if (finalBody.get("vehicleNo") == null || "".equals(finalBody.get("vehicleNo"))) {
					vehicleNo = "";
				} else {
					vehicleNo = finalBody.get("vehicleNo").toString();
				}
				System.out.println("check 2");
				JSONObject resultObj = new JSONObject();
				resultObj.put("itemSize", itemSize);
				resultObj.put("invoiceNo", invoiceNo);
				resultObj.put("invoiceDate", invoiceDate);
				resultObj.put("invoiceValue", invoiceValue);
				resultObj.put("ewayBillId", ewayBillId);
				resultObj.put("ewayBillNo", ewayBillNo);
				resultObj.put("ewayBillDate", ewayBillDate);
				resultObj.put("ewayBillExpiry", ewayBillExpiry);
				resultObj.put("ewayBillUpdatedDate", ewayBillUpdatedDate);
				resultObj.put("consignorGst", consignorGst);
				resultObj.put("consignorName", consignorName);
				resultObj.put("consignorAddress", consignorAddress);
				resultObj.put("consignorPlace", consignorPlace);
				resultObj.put("consignorPincode", consignorPincode);
				resultObj.put("consignorState", consignorState);
				resultObj.put("consigneeGst", consigneeGst);
				resultObj.put("consigneeName", consigneeName);
				resultObj.put("consigneeAddress", consigneeAddress);
				resultObj.put("consigneePlace", consigneePlace);
				resultObj.put("consigneePincode", consigneePincode);
				resultObj.put("consigneeState", consigneeState);
				resultObj.put("transId", transId);
				resultObj.put("transName", transName);
				resultObj.put("transDocNo", transDocNo);
				resultObj.put("transDocDate", transDocDate);
				resultObj.put("vehicleNo", vehicleNo);
				resultObj.put("status", "1");
				resultObj.put("message", "success");
				returnArray.add(resultObj);
			} else {
				JSONObject errorObj = new JSONObject();
				errorObj.put("status", "0");
				errorObj.put("message", "EwayBill No Not Found");
				returnArray.add(errorObj);
			}
			System.out.println("hey received");
		} catch (Exception e) {
			System.out.println("cant process this message..........." + e.getMessage());
		}
		return returnArray;
	}

	@GetMapping(path = "/getOrderlist", produces = "application/json")
	public JSONArray getOrderlist(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "desigId") String desigId) {

		JSONArray returnArray = new JSONArray();

		try {
			System.out.println("userId..in getRunSheetDetails== " + userId);
			System.out.println("desigId..in getRunSheetDetails=== " + desigId);
			returnArray = hsServiceBP.getOrderlist(userId, desigId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Controller Orderlist => " + returnArray);

		return returnArray;
	}

	@GetMapping(path = "/getManualnolist", produces = "application/json")
	public JSONArray getManualnolist(@RequestParam(value = "BranchId") String BranchId) {

		JSONArray returnArray = new JSONArray();

		try {
			System.out.println("BranchId..in getManualnolist== " + BranchId);
			returnArray = hsServiceBP.getManualnolist(BranchId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) { 
			e.printStackTrace();
		}

		return returnArray;
	}

	@GetMapping(path = "/getSKUNamelist", produces = "application/json")
	public JSONArray getSKUNamelist(@RequestParam(value = "BranchId") String BranchId) {

		JSONArray returnArray = new JSONArray();

		try {
			System.out.println("BranchId..in getSKUNamelist== " + BranchId);
			returnArray = hsServiceBP.getSKUNamelist(BranchId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnArray;
	}

//	@GetMapping(path = "/checkInvoicevalState", produces = "application/json")
//	public JSONArray checkInvoicevalState(@RequestParam(value = "state") String state) {
//
//		JSONArray returnArray = new JSONArray();
//		try {
//			if (returnArray == null) {
//
//				returnArray = hsServiceBP.checkInvolicevalState(state);
//				JSONObject jsonObject = new JSONObject();
//				jsonObject.put("status", "Data Not Available");
//				returnArray.add(jsonObject);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return returnArray;
//	}

	@GetMapping(path = "/getPakaginglist", produces = "application/json")
	public JSONArray getPakaginglist() {

		JSONArray returnArray = new JSONArray();
		

		try {
//			System.out.println("BranchId..in getSKUNamelist== " + );
			returnArray = hsServiceBP.getPakaginglist();
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnArray;
	}
	
	@PostMapping(path ="/Insertarticle" , produces = "application/json")
	public JSONArray Insertarticle(@RequestParam(value = "waybillId")String waybillId,
			@RequestParam(value="userId")String userId,@RequestBody String Objects) {
		JSONArray resultArray = new JSONArray();
		JSONObject responseobj = new JSONObject();
		try {
			
			
			String SKU_name = "";
			String Pkgtype = "";
			String nospkg = "";
			String nossku = "";
			String Length = "";
			String Breadth = "";
			String Height = "";
			String per_piece_weight = "";
			String WayBill_Mode ="";
			String Rate_Type = "";
			Long UomType = null ;
			Long weight_type = null;
			String SKU_Id = "";
			String SaidtoContianId = "";
			String volumetricweight = "";
			String RateType = "";
			
			JSONArray jsonobjs = (JSONArray) new JSONParser().parse(Objects);			
			System.out.println("postOrderDetails requestString=" + jsonobjs);
			for(int i=0;i<jsonobjs.size();i++) {
				
				JSONObject jsonobj = (JSONObject) jsonobjs.get(i);
				
				
				Pkgtype = (String) jsonobj.get("Pkgtype");
				SaidtoContianId = (String) jsonobj.get("SaidtoContianId");
				nospkg = (String) jsonobj.get("nospkg");
				nossku = (String) jsonobj.get("nossku");
				Length = (String) jsonobj.get("Length");
				Breadth = (String) jsonobj.get("Breadth");
				Height = (String) jsonobj.get("Height");
				WayBill_Mode = (String) jsonobj.get("WayBill_Mode");
				volumetricweight = (String) jsonobj.get("volumetricweight");
//				Rate_Type = (String) jsonobj.get("Rate_Type");
				UomType = (Long) jsonobj.get("UomType");
				weight_type = (Long) jsonobj.get("weight_type");
				SKU_Id = (String) jsonobj.get("SKUNameId");
				per_piece_weight = (String) jsonobj.get("per_piece_weight");
				RateType = (String) jsonobj.get("RateType");
				
			}
			int status = hsServiceBP.Insertarticle(waybillId, userId,
													SKU_Id,SaidtoContianId,
													nospkg,nossku,
													Length,Breadth,Height,
													UomType,weight_type,per_piece_weight,WayBill_Mode,
													RateType,volumetricweight);
			
			
			if (status > 0) {					
				responseobj.put("Code", "1");
				responseobj.put("Status", "Success");
				resultArray.add(responseobj);
			}else {
				responseobj.put("Code", "0");
				responseobj.put("Status", "Failed to Insert articles");	
				resultArray.add(responseobj);
				}
			
		}catch(Exception e) {
			
			e.printStackTrace();
			System.out.println("++++++++++ " + e.getMessage());
			responseobj.put("Code", "0");
			responseobj.put("Status", "Aricle can't be added");	
			resultArray.add(responseobj);
			
		}
		
		return resultArray;
	}
	

	@GetMapping(path = "/getArticlelist", produces = "application/json")
	public JSONArray getArticlelist(@RequestParam(value = "waybillId") String waybillId) {

		JSONArray returnArray = new JSONArray();
		

		try {
//			System.out.println("BranchId..in getSKUNamelist== " + );
			returnArray = hsServiceBP.getArticlelist(waybillId);
			if (returnArray == null) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Data Not Available");
				returnArray.add(jsonObject);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Controller retrn Articles == > " +returnArray );

		return returnArray;
	}
	
	
//		@PutMapping(path="/confirmArticles",produces ="application/json")
//		public JSONArray confirmArticles(@RequestParam(value="waybilId") String waybilId,
//									@RequestParam (value="UserId") String UserId) {
//			JSONArray resultArray = new JSONArray();
//			JSONObject responseobj = new JSONObject(); 
//			
//					try {
//					int status = hsServiceBP.confirmArticles(waybilId, UserId);
//					
//					
//					if (status > 0) {					
//						responseobj.put("Code", "1");
//						responseobj.put("Status", "Success");
//						resultArray.add(responseobj);
//					}else {
//						responseobj.put("Code", "0");
//						responseobj.put("Status", "Failed to Update articles");	
//						resultArray.add(responseobj);
//						}
//					
//				}catch(Exception e) {
//					
//					e.printStackTrace();
//					System.out.println("++++++++++ " + e.getMessage());
//					responseobj.put("Code",  e.getMessage());
//					responseobj.put("Status", "Aricle can't be Update");	
//					resultArray.add(responseobj);
//					
//				}
//				
//				return resultArray;
//			
//		}
		
		
		
		
		@PostMapping(path ="/Insertewaybilllist" , produces = "application/json")
		public JSONArray Insertewaybilllist(@RequestParam(value = "waybillId")String waybillId,
				@RequestParam(value="userId")String userId,@RequestBody String Objects) {
			JSONArray resultArray = new JSONArray();
			JSONObject responseobj = new JSONObject();
			try {
				String ewaybill_id = "";
				String ewaybill_no = "";
				String ewaybill_date = "";
				String ewaybill_expiry_date = "";
				String invoice_no = "";
				String invoice_date = "";
				String invoice_value = "";
				String vehicle_id = "";
				String vehicle_no = "";
				String status = "";
				String post_status = "";
				String from_gst_no = "";
				String to_gst_no = "";
				String from_state = "";
				String to_state = "";
				
				JSONArray jsonobjs = (JSONArray) new JSONParser().parse(Objects);			
				System.out.println("ewaybill requestString=" + jsonobjs);
				for(int i=0;i<jsonobjs.size();i++) {
					
					JSONObject jsonobj = (JSONObject) jsonobjs.get(i);
					System.out.println("Ewaybill Id == > " + jsonobj + "and" + jsonobj.get("ewaybill_id") );
					
					ewaybill_id = (String) jsonobj.get("ewaybill_id");
					ewaybill_no = (String) jsonobj.get("ewaybill_no");
					ewaybill_date = (String) jsonobj.get("ewaybill_date");
					ewaybill_expiry_date = (String) jsonobj.get("ewaybill_expiry_date");
					invoice_no = (String) jsonobj.get("invoice_no");
					invoice_date = (String) jsonobj.get("invoice_date");
					invoice_value = (String) jsonobj.get("invoice_value");
					vehicle_id = (String) jsonobj.get("vehicle_id");
					vehicle_no = (String) jsonobj.get("vehicle_no");
					status = (String) jsonobj.get("status");
					post_status = (String) jsonobj.get("post_status");
					from_gst_no = (String) jsonobj.get("from_gst_no");
					to_gst_no = (String) jsonobj.get("to_gst_no");
					from_state = (String) jsonobj.get("from_state");
					to_state = (String) jsonobj.get("to_state");

//					System.out.println("Waybill insetst As BP datas === > " + ewaybill_id+ ewaybill_no+ 
//							ewaybill_date+ ewaybill_expiry_date+ 
//							invoice_no+invoice_date+ 
//							invoice_value+ vehicle_id + 
//
//							vehicle_no+ status+ 
//							post_status+ from_gst_no+
//							to_gst_no+from_state+ to_state);
				}
				int opstatus = hsServiceBP.Insertewaybilllist(ewaybill_id,
						ewaybill_no,
						ewaybill_date,
						ewaybill_expiry_date,
						invoice_no,
						invoice_date,
						invoice_value,
						vehicle_id,
						vehicle_no,
						status,
						post_status,
						from_gst_no,
						to_gst_no,
						from_state,
						to_state);
				
				System.out.println("Waybill insetst As BP datas === > " + ewaybill_id+ ewaybill_no+ 
						ewaybill_date+ ewaybill_expiry_date+ 
						invoice_no+invoice_date+ 
						invoice_value+ vehicle_id + 

						vehicle_no+ status+ 
						post_status+ from_gst_no+
						to_gst_no+from_state+ to_state);
				if (opstatus > 0) {					
					responseobj.put("Code", "1");
					responseobj.put("Status", "Success");
					resultArray.add(responseobj);
				}else {
					responseobj.put("Code", "0");
					responseobj.put("Status", "Failed to Insert articles");	
					resultArray.add(responseobj);
					}
				
			}catch(Exception e) {
				
				e.printStackTrace();
				System.out.println("++++++++++ " + e.getMessage());
				responseobj.put("Code", "0");
				responseobj.put("Status", "Aricle can't be added");	
				resultArray.add(responseobj);
				
			}
			
			return resultArray;
		}
		
		@GetMapping(path="/Deletearticles",produces ="application/json")
		public JSONArray Deletearticles(@RequestParam(value="waybilId") String waybilId) {
			JSONArray resultArray = new JSONArray();
			JSONObject responseobj = new JSONObject(); 
			
					try {
					int status = hsServiceBP.Deletearticles(waybilId);
					
					
					if (status > 0) {					
						responseobj.put("Code", "1");
						responseobj.put("Status", "Success");
						resultArray.add(responseobj);
					}else {
						responseobj.put("Code", "0");
						responseobj.put("Status", "Failed to Delete articles");	
						resultArray.add(responseobj);
						}
					
				}catch(Exception e) {
					
					e.printStackTrace();
					System.out.println("++++++++++ " + e.getMessage());
					responseobj.put("Code",  e.getMessage());
					responseobj.put("Status", "Aricle can't be Deleted");	
					resultArray.add(responseobj);
					
				}
				
				return resultArray;
			
		}
		
		@GetMapping(path="/GetArticleStatus",produces ="application/json")
		public JSONArray GetArticleStatus(@RequestParam(value="waybilnum") String waybilnum) {
			JSONArray resultArray = new JSONArray();
			JSONObject responseobj = new JSONObject(); 
			
					try {
						resultArray = hsServiceBP.GetArticleStatus(waybilnum);
					
						if (resultArray == null || resultArray.size() == 0 ) {
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("Article_status", null);
							resultArray.add(jsonObject);
						}
					
				}catch(Exception e) {
					
					e.printStackTrace();
					System.out.println("++++++++++ " + e.getMessage());
					responseobj.put("Code",  e.getMessage());
					responseobj.put("Status", "Status not available");	
					resultArray.add(responseobj);
					
				}
				System.out.println("Hey am using there " +resultArray);
				return resultArray;
			
		}
		
		@PostMapping(path ="/Insertoperationwaybill" , produces = "application/json")
		public JSONArray Insertoperationwaybill(@RequestParam(value = "ewaybillId")String ewaybillId,
				@RequestParam(value="userId")String userId,@RequestBody String Objects) {
			JSONArray resultArray = new JSONArray();
			JSONObject responseobj = new JSONObject();
			try {
				String ewaybill_no = "";
				String invoice_no = "";
				String ewaybill_date = "";
				long waybilltype = 0;
				String customername = "";
				String Cust_id = "";
				String Consignor_Name = "";
				String ConsigneeName = "";
				String Actualweight = "";
				String ewaybill_expiry_date = "";
				long movmenttype = 0;
				long weighttype = 0;
				String RequestID = "";
				String appoinment = "";
				String appointment_date_time = "";
				String floor_applicable = "";
				String no_of_floors = "";
				long coddod_applicable = 0;
				long Specialratetype = 0;
				long barcodeType = 0;

				String mannualnum = "";
				
				JSONArray jsonobjs = (JSONArray) new JSONParser().parse(Objects);			
				System.out.println("ewaybill requestString=" + jsonobjs);
				for(int i=0;i<jsonobjs.size();i++) {
					
					JSONObject jsonobj = (JSONObject) jsonobjs.get(i);
					
					ewaybill_no = (String) jsonobj.get("ewaybill_no");
					invoice_no = (String) jsonobj.get("invoice_no");
					ewaybill_date = (String) jsonobj.get("ewaybill_date");
					waybilltype = (long) jsonobj.get("waybilltype");
					customername = (String) jsonobj.get("customername");
					Cust_id = (String) jsonobj.get("Cust_id");
					Consignor_Name = (String) jsonobj.get("Consignor_Name");
					ConsigneeName = (String) jsonobj.get("ConsigneeName");
					Actualweight = (String) jsonobj.get("Actualweight");
					ewaybill_expiry_date = (String) jsonobj.get("ewaybill_expiry_date");
					movmenttype = (long) jsonobj.get("movmenttype");
					weighttype = (long) jsonobj.get("weighttype");
					RequestID = (String) jsonobj.get("RequestID");
					appoinment = (String) jsonobj.get("appoinment");
							appointment_date_time = (String) jsonobj.get("appointment_date_time");
							floor_applicable = (String) jsonobj.get("floor_applicable");
							no_of_floors = (String) jsonobj.get("no_of_floors");
							coddod_applicable = (long) jsonobj.get("coddod_applicable");
							Specialratetype = (long) jsonobj.get("Specialratetype");
							barcodeType = (long) jsonobj.get("barcodeType");
							mannualnum = (String) jsonobj.get("mannualnum");
					
				}
				int opstatus = hsServiceBP.Insertoperationwaybill(
						ewaybill_no,
						ewaybillId,
						userId,
	                    invoice_no,
	                    ewaybill_date,
	                    waybilltype,
	                    customername,
	                    Cust_id,
	                    Consignor_Name,
	                    ConsigneeName,
	                    Actualweight,
	                    ewaybill_expiry_date,
	                    movmenttype,
	                    weighttype,
	                    RequestID,
	                    appoinment,
	                    appointment_date_time,
	                    floor_applicable,
	                    no_of_floors,
	                    coddod_applicable,
	                    Specialratetype,
	                    mannualnum,barcodeType);
				if (opstatus > 0) {					
					responseobj.put("Code", "1");
					responseobj.put("Status", "Success");
					resultArray.add(responseobj);
				}else {
					responseobj.put("Code", "0");
					responseobj.put("Status", "Failed to Insert articles");	
					resultArray.add(responseobj);
					}
				
			}catch(Exception e) {
				
				e.printStackTrace();
				System.out.println("++++++++++ " + e.getMessage());
				responseobj.put("Code", "0");
				responseobj.put("Status", "Aricle can't be added");	
				resultArray.add(responseobj);
				
			}
			
			return resultArray;
		}

		
		
		@PostMapping(path = "/updateReportingDetails",  produces = "application/json")
		public JSONArray updateReportingDetails(@RequestParam(value = "tripId") String tripId,@RequestParam(value = "substatusId") String substatusId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateReportingDetails(tripId,substatusId,reportingDate,repotingTime);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		
		
		
		
		@PostMapping(path = "/updateLoadingDetails",  produces = "application/json")
		public JSONArray updateLoadingDetails(@RequestParam(value = "tripId") String tripId,@RequestParam(value = "substatusId") String substatusId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateLoadingDetails(tripId,substatusId,reportingDate,repotingTime);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		
		@PostMapping(path = "/updateGateoutDetails",  produces = "application/json")
		public JSONArray updateGateoutDetails(@RequestParam(value = "tripId") String tripId,@RequestParam(value = "substatusId") String substatusId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime,
				@RequestParam(value = "odometerReading") String odometerReading) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateGateoutDetails(tripId,substatusId,reportingDate,repotingTime,odometerReading);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		@PostMapping(path = "/updateDestinationReachedDetails",  produces = "application/json")
		public JSONArray updateDestinationReachedDetails(@RequestParam(value = "tripId") String tripId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateDestinationReachedDetails(tripId,reportingDate,repotingTime);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		@PostMapping(path = "/updateUnloadingDetails",  produces = "application/json")
		public JSONArray updateUnloadingDetails(@RequestParam(value = "tripId") String tripId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateUnloadingDetails(tripId,reportingDate,repotingTime);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		@PostMapping(path = "/updateTripEnd",  produces = "application/json")
		public JSONArray updateTripEnd(@RequestParam(value = "tripId") String tripId,
//				@RequestParam(value = "substatusId") String substatusId,
				@RequestParam(value = "reportingDate") String reportingDate,
				@RequestParam(value = "repotingTime") String repotingTime) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateTripEnd(tripId,reportingDate,repotingTime);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		@PostMapping(path = "/updateDocument", produces = "application/json")
		public JSONArray updateDocument(
			    @RequestParam(value = "tripId") String tripId,
				@RequestParam(name = "userId") String userId,
				@RequestBody String requestString) 
				{ 

			JSONArray returnArray = new JSONArray();
			
			try {
				
			
				JSONArray jsonobjs = (JSONArray) new JSONParser().parse(requestString);		
				
				JSONObject jsonobj = (JSONObject) jsonobjs.get(0);
				
				JSONArray innerArry = (JSONArray) jsonobj.get("podImage");
				
//							System.out.println("innerArry=="+innerArry);
				returnArray = hsServiceBP.updateDocument(tripId,innerArry,userId);			
				
				
				
				if (returnArray == null) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("status", "0");
					jsonObject.put("Remarks", "Data Not Updated");
					returnArray.add(jsonObject);
				}	
				
				
				
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			return returnArray;
		}
		
		
		
		@PostMapping(path = "/updateTripPod", produces = "application/json")
		public JSONArray updateTripPod(
			    @RequestParam(value = "tripId") String tripId,
				@RequestParam(name = "userId") String userId,
				@RequestBody String requestString) 
				{ 

			JSONArray returnArray = new JSONArray();
			
			try {
				
			
				JSONArray jsonobjs = (JSONArray) new JSONParser().parse(requestString);		
				
				JSONObject jsonobj = (JSONObject) jsonobjs.get(0);
				
				JSONArray innerArry = (JSONArray) jsonobj.get("podImage");
				
//							System.out.println("innerArry=="+innerArry);
				returnArray = hsServiceBP.updateTripPod(tripId,innerArry,userId);			
				
				
				
				if (returnArray == null) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("status", "0");
					jsonObject.put("Remarks", "Data Not Updated");
					returnArray.add(jsonObject);
				}	
				
				
				
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			return returnArray;
		}
		
		
		@GetMapping(path = "/getInterimTripList",  produces = "application/json")
		public JSONArray getInterimTripList(@RequestParam(value = "tripId") String tripId) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
//				String getEmpId = hsServiceBP.getEmpId(userId);
//				System.out.println("empId====="+getEmpId);
//				if(getEmpId != null) {
				System.out.println("tripId-======"+tripId);
					returnArray = hsServiceBP.getInterimTripList(tripId);
//					System.out.println("returnArray=="+returnArray);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
//				}
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		@PostMapping(path = "/updateInterimGateInReportingDetails",  produces = "application/json")
		public JSONArray updateInterimGateInReportingDetails(@RequestParam(value = "tripId") String tripId,@RequestParam(value = "pointId") String pointId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime,
				@RequestParam(value = "routeOrder") String routeOrder) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateInterimGateInReportingDetails(tripId,pointId,reportingDate,repotingTime,routeOrder);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		@PostMapping(path = "/updateInterimGateOutReportingDetails",  produces = "application/json")
		public JSONArray updateInterimGateOutReportingDetails(@RequestParam(value = "tripId") String tripId,@RequestParam(value = "pointId") String pointId,
				@RequestParam(value = "reportingDate") String reportingDate,@RequestParam(value = "repotingTime") String repotingTime,
				@RequestParam(value = "routeOrder") String routeOrder) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				
					returnArray = hsServiceBP.updateInterimGateOutReportingDetails(tripId,pointId,reportingDate,repotingTime,routeOrder);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
				
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
		
		
		
		@PostMapping(path = "/uploadInterimPod", produces = "application/json")
		public JSONArray uploadInterimPod(
			    @RequestParam(value = "tripId") String tripId,
				@RequestParam(name = "userId") String userId,
				@RequestParam(name = "pointId") String pointId,
				@RequestParam(name = "routeOrder") String routeOrder,
				@RequestBody String requestString) 
				{ 

			JSONArray returnArray = new JSONArray();
			
			try {
				
			
				JSONArray jsonobjs = (JSONArray) new JSONParser().parse(requestString);		
				
				JSONObject jsonobj = (JSONObject) jsonobjs.get(0);
				
				JSONArray innerArry = (JSONArray) jsonobj.get("podImage");
				
//							System.out.println("innerArry=="+innerArry);
				returnArray = hsServiceBP.uploadInterimPod(tripId,innerArry,userId,pointId,routeOrder);			
				
				
				
				if (returnArray == null) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("status", "0");
					jsonObject.put("Remarks", "Data Not Updated");
					returnArray.add(jsonObject);
				}	
				
				
				
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			return returnArray;
		}
		
		
		
		@GetMapping(path = "/getWaybillPdf",  produces = "application/json")
		public JSONArray getWaybillPdf(@RequestParam(value = "waybillId") String waybillId) {
			JSONArray returnArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			try {
				
				System.out.println("tripId-======"+waybillId);
					returnArray = hsServiceBP.getWaybillPdf(waybillId);
//					System.out.println("returnArray=="+returnArray);
					if (returnArray == null) {
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("status", "Data Not Available");
						returnArray.add(jsonObject1);
					}
					
					
//				}
			}catch (Exception e) {
				e.printStackTrace();		
			}
			
			
			return returnArray;
			
		}
}
